jQuery(document).ready(function($) {

	const ratio = "three-two";

	const referals = {
		'o': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'i': {
			'box': 'a',
			'orientation': 'portrait'
		},
		/**
		 * 2 images
		 */
		'oo': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'ii': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'oi': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'io': {
			'box': 'a',
			'orientation': 'portrait'
		},
		/**
		 * 3 images
		 */
		'ooo': {
			'box': 'c',
			'orientation': 'landscape'
		},
		'ioo': {
			'box': 'b',
			'orientation': 'landscape'
		},
		'oio': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'ooi': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'oii': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'ioi': {
			'box': 'b',
			'orientation': 'landscape'
		},
		'iio': {
			'box': 'c',
			'orientation': 'landscape'
		},
		'iii': {
			'box': 'a',
			'orientation': 'portrait'
		},
		/**
		 * 4 images
		 */
		'oooo-v0': {
			'box': 'c',
			'orientation': 'landscape'
		},
		'oooo-v1': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'oooo-v2': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'oioo': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'oooi': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'iooo': {
			'box': 'd',
			'orientation': 'landscape'
		},
		'ooio': {
			'box': 'd',
			'orientation': 'landscape'
		},
		'oooi': {
			'box': 'a',
			'orientation': 'landscape'
		},
		'iiii': {
			'box': 'a',
			'orientation': 'portrait'
		},
		/**
		 * 5 images
		 */
		'aoooo': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'ioooo': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'ooioo': {
			'box': 'c',
			'orientation': 'portrait'
		},
		'ooooi': {
			'box': 'e',
			'orientation': 'portrait'
		},
		'iiooo': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'iooio': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'iiooo': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'ooiio': {
			'box': 'e',
			'orientation': 'landscape'
		},
		'ooioi': {
			'box': 'c',
			'orientation': 'portrait'
		},
		'oooii': {
			'box': 'd',
			'orientation': 'portrait'
		},
		'oiioo': {
			'box': 'b',
			'orientation': 'portrait'
		},
		'oiooi': {
			'box': 'b',
			'orientation': 'portrait'
		},
		'iiioo': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'iiooi': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'iooii': {
			'box': 'a',
			'orientation': 'portrait'
		},
		'ooiii': {
			'box': 'c',
			'orientation': 'portrait'
		}
	};

	function getHeightByWidth(ratio, width, orientation) {
		if(orientation == 'landscape') {
			switch (ratio) {
				case 'three-two':
					return (2 * width) / 3;
					break;
				case 'five-four':
					return (4 * width) / 5;
					break;
			}
		} else {
			switch (ratio) {
				case 'three-two':
					return (3 * width) / 2;
					break;
				case 'five-four':
					return (5 * width) / 4;
					break;
			}
		}
	}

	function tilesCalculateRow() {
		$('.mgl-tiles').each(function() {
			$(this).find('.mgl-row').each(function() {
				var layout = $(this).attr('data-tiles-layout');
				var ref = referals[layout];
				var $ref = $(this).find('.mgl-box.'+ref.box);
				var ref_width = $ref.outerWidth();
				$(this).css('height', getHeightByWidth(ratio, ref_width, ref.orientation));
			});
		});
	}

	$(window).resize(function() {
		tilesCalculateRow();
	});

	$(document.body).on('post-load', function() {
		tilesCalculateRow();
	});

	window.mglCalculateRow = tilesCalculateRow;
	window.mglCalculateRow();
});