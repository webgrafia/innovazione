<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ministero
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php echo get_bloginfo('name'); ?> <?php wp_title(); ?></title>
    <?php
    if(pll_current_language() != "it"){
    ?>
    <meta name="robots" content="noindex">
    <?php
    }
    ?>
    <!-- Favicon -->
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png"> <!-- Windows 8 -->

    <link rel="icon"  type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon.ico">
    <link rel="icon" sizes="16x16" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-chrome-16x16.png">
    <link rel="icon" sizes="32x32" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-chrome-32x32.png">
    <link rel="icon" sizes="192x192" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-chrome-192x192.png">
    <link rel="icon" sizes="512x512" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-chrome-512x512.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<header id="site-header">

    <nav class="navbar navbar-mobile d-lg-none">
        <div class="container">
            <div class="col-7 px-0">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                    <img src="<?php bloginfo("template_url"); ?>/assets/img/logo-or.svg" class="img-fluid" alt="MadeIt" />
                </a>
            </div>
            <div class="col-5 px-0 text-right">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only" id="label-menu">Menu</span>
                    <span class="navbar-toggler-inner"></span>
                </button>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-desktop navbar-expand-lg" aria-labelledby="label-menu">
        <div class="container">
            <div class="row">

                <div class="col-6 col-lg-2 d-none d-lg-block">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <img src="<?php bloginfo("template_url"); ?>/assets/img/logo-or.svg" class="img-fluid" alt="MadeIt" />
                    </a>
                </div>
                <div class="col-6 col-lg-10 text-right">
                    <div class="collapse navbar-collapse" id="main-menu">
                        <div class="navs-wrap">
							<?php
							wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'menu_class' => 'navbar-nav mr-lg-0 ml-lg-auto',  'walker' => new madeit_walker()) );
							?>

                            <div class="lang"><?php
                                if(function_exists("pll_the_languages"))
                                    pll_the_languages( array( 'dropdown' => 1, 'display_names_as' => "name", 'show_flags' => 1 ));
                                ?>
                            </div>

                            <?php
							/*
								<ul class="lang">
									<li class="current">
										<a class="lang-item">ITA</a>
										<ul class="submenu">
											<li class="lang-item lang-item-2 lang-item-it current-lang lang-item-first">
												<a hreflang="it-IT" href="http://178.62.6.197/innovazione/madeit/" class="" lang="it-IT">ITA</a>
											</li>
											<li class="lang-item lang-item-5 lang-item-en">
												<a hreflang="en-GB" href="http://178.62.6.197/innovazione/madeit/en/home-2/" lang="en-GB">EN</a>
											</li>
										</ul>
									</li>
								</ul>
							*/
							?>

                            <div class="social-menu text-center">
                                <div class="d-inline-block d-lg-none text-left">
                    		    <span class="nav-intro">
		                            <?php echo get_field("testo_social", "options"); ?>
		                        </span>
                                    <ul class="nav nav-social nav-social--round invert justify-content-lg-end">
										<?php  get_template_part("template-parts/common/social");  ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </nav>
</header>


