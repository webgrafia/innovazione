<?php

/**
 * Search for a page with a particular template.
 *
 * @param string $template Template filename.
 * @param array  $args     (Optional) See also get_posts() for example parameter usage.
 * @param bool   $single   (Optional) Whether to return a single value.
 *
 * @return Will be an array of WP_Post if $single is false. Will be a WP_Post object if the page is find, FALSE otherwise
 */
if (!function_exists('get_page_by_template')) {
	function get_page_by_template($template, $args = array(), $single = true) {
		$pages_by_template = wp_cache_get('pages_by_template', 'madeit');
		if (empty($pages_by_template) || !is_array($pages_by_template)) {
			$pages_by_template = array();
		}
		if (!isset($pages_by_template[$template])) {
			$args = wp_parse_args(array(
				'posts_per_page' => -1,
				'post_type'      => 'page',
				'suppress_filters'  => 0,
				'meta_query'     => array(
					array(
						'key'   => '_wp_page_template',
						'value' => $template,
					),
				),
			), $args);
			$pages = get_posts($args);
			$pages_by_template[$template]= array(
				'single' => !empty($pages) && is_array($pages) ? reset($pages) : false,
				'pages'  => $pages,
			);
		}
		wp_cache_set('pages_by_template', $pages_by_template, 'madeit');
		return $pages_by_template[$template][$single ? 'single' : 'pages'];
	}
}


/**
 * //Custom pagination
 */
function madeit_bootstrap_pagination()
{
	if (is_singular())
		return;
	global $wp_query;
	/** Stop execution if there's only 1 page */
	if ($wp_query->max_num_pages <= 1)
		return;
	$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
	$max = intval($wp_query->max_num_pages);
	/** Add current page to the array */
	if ($paged >= 1)
		$links[] = $paged;
	/** Add the pages around the current page to the array */
	if ($paged >= 3) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if (($paged + 2) <= $max) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<nav class="w-100 pagination-wrapper justify-content-center" aria-label="Paginazione">' . "\n";
	echo '<ul class="pagination">' . "\n";
	/** Previous Post Link */
	if (get_previous_posts_link()){
		$prv_post = previous_posts( false );
		echo '<li  class="page-item"><a class="page-link" href="'.($prv_post).'"><svg class="icon icon-primary"><use xlink:href="'.get_template_directory_uri().'/assets/bootstrap-italia/svg/sprite.svg#it-chevron-left"></use></svg>
<span class="sr-only">Pagina precedente</span></a></li>';
	}

	/** Link to first page, plus ellipses if necessary */
	if (!in_array(1, $links)) {
		$class = 1 == $paged ? ' class="page-item active"' : ' class="page-item" ';
		$num = 1 == $paged ? " <span class=\"d-inline-block d-sm-none\">Pagina </span>1" : "1";
		printf('<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), $num);
		if (!in_array(2, $links))
			echo '<li class="page-item"><span class="page-link">...</span></li>' . "\n";
	}
	/** Link to current page, plus 2 pages in either direction if necessary */
	sort($links);
	foreach ((array)$links as $link) {
		$class = $paged == $link ?  ' class="page-item active"' : ' class="page-item" ';
		$aria = $paged == $link ?  ' aria-current="page" ' : ' ';
		$num = $paged == $link  ? " <span class=\"d-inline-block d-sm-none\">Pagina </span>".$link : $link;
		printf('<li%s><a class="page-link" %s href="%s">%s</a></li>' . "\n", $class, $aria, esc_url(get_pagenum_link($link)), $num);
	}
	/** Link to last page, plus ellipses if necessary */
	if (!in_array($max, $links)) {
		if (!in_array($max - 1, $links))
			echo '<li class="page-item"><span class="page-link">...</span></li>' . "\n";

		$class = $paged == $max ? ' class="page-item active"' : ' class="page-item" ';
		$num = $paged == $max  ? " <span class=\"d-inline-block d-sm-none\">Pagina </span>".$max : $max;
		printf('<li%s><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $num);
	}

	/** Next Post Link */
	if (get_next_posts_link()){
		$nxt_post = next_posts(0,false);
		echo '<li  class="page-item"><a class="page-link" href="'.($nxt_post).'"> <span class="sr-only">Pagina successiva</span><svg class="icon icon-primary"><use xlink:href="'.get_template_directory_uri().'/assets/bootstrap-italia/svg/sprite.svg#it-chevron-right"></use></svg>
</a></li>';
	}

	echo '</ul>' . "\n";
	echo '</nav>' . "\n";
	echo '<!--/.Pagination-->' . "\n";
}


if(!function_exists("madeit_get_post_related")){
	function madeit_get_post_related($limit){
		global $post;

		$categories = get_the_category($post->ID);
		$category_ids = array();
		if ($categories) {
			foreach ( $categories as $individual_category ) {
				$category_ids[] = $individual_category->term_id;
			}
		}

		$related = get_posts(
			array(
				'post_type' => "post",
				'category__or' => $category_ids,
				'numberposts'  => $limit,
				'post__not_in' => array( $post->ID )
			)
		);


		return $related;
	}
}



/**
 *  Walker per il menu principale
 */
class madeit_walker extends Walker_Nav_Menu
{
	private $el;
	function start_el(&$output, $item, $depth=0, $args=NULL, $id=0)
	{
		global $wp_query;
		$this->el = $item;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		if(in_array("current-menu-item", $classes)){
			$classes[]="active";
		}
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = '  class="'. esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= ' class="nav-link" ';


		$prepend = "\n";
		$append = "\n";
		$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

		if($depth != 0)
		{
			$description = $append = $prepend = "";
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
		$item_output .= $description.$args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

		if ($item->menu_order == 1) {
			$classes[] = 'first';
		}

	}
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = str_repeat( $t, $depth );
		$output .= "{$n}{$indent}<ul class=\"dropdown-menu\" aria-labelledby=\"link-".$this->el->ID."\">{$n}";
	}

}



/**
 * contest status
 * prevedo 6 stati:
 * pre-iscrizione - progress-iscrizione - post-iscrizione
 * pre-contest - progress-contest - post-contest
 * @param $post
 *
 * @return array
 */
// chiuso, aperto, in progress
function madeit_contest_status($post){
	$ret = array();
	$now = time();
	$inizio_iscrizioni = strtotime(str_replace("/", "-", get_field("inizio_iscrizioni", $post)));
	$termine_iscrizioni = strtotime(str_replace("/", "-", get_field("termine_iscrizioni", $post)));
	$data_inizio_contest = strtotime(str_replace("/", "-", get_field("data_inizio_contest", $post)));
	$data_fine_contest = strtotime(str_replace("/", "-", get_field("data_fine_contest", $post)));


	if($data_inizio_contest > $now ){
		$ret[]="pre-contest";
	}else if(($data_inizio_contest <= $now ) && ($now < $data_fine_contest) ){
		$ret[]="progress-contest";
	}else if($data_fine_contest <= $now){
		$ret[]="post-contest";
	}

	if($inizio_iscrizioni > $now ){
		$ret[]="pre-iscrizione";
	}else if(($inizio_iscrizioni <= $now ) && ($now <= $termine_iscrizioni) ){
		$ret[]="progress-iscrizione";
	}else if($termine_iscrizioni < $now){
		$ret[]="post-iscrizione";
	}


	return $ret;
}

/**
 * Ordinamento dei contest: prima quelli che devono ancora iniziare in ordine crescente, poi quelli tracorsi in ordine decrescente.
 * Richiede 2 query
 * @param int $limit
 *
 */
function madeit_list_contests($limit = -1){

	$meta_query_next = array(
		array(
			'key' => 'data_inizio_contest',
			'value' => date('Ymd'),
			'type' => 'DATE',
			'compare' => '>='
		)
	);
	$args_next = array(
		'post_type' => "contest",
		"posts_per_page" => $limit,
		"meta_query" => $meta_query_next,
		"meta_key" => "data_inizio_contest",
		'orderby' => 'meta_value_num',
		'order' => "ASC"
	);
	$contests_next = get_posts($args_next);

	$meta_query_prev = array(
		array(
			'key' => 'data_inizio_contest',
			'value' => date('Ymd'),
			'type' => 'DATE',
			'compare' => '<'
		)
	);
	$args_prev = array(
		'post_type' => "contest",
		"posts_per_page" => $limit,
		"meta_query" => $meta_query_prev,
		"meta_key" => "data_inizio_contest",
		'orderby' => 'meta_value_num',
		'order' => "DESC"
	);
	$contests_prev = get_posts($args_prev);


	$result = array_merge($contests_next, $contests_prev);
	if($limit != "-1"){
		$result = array_slice($result, 0, $limit);
	}
	return $result;
}