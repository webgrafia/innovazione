<?php

// Register Custom Post Type
function madeit_register_post_type() {


	// incentivo
	$labels = array(
		'name'                  => _x( 'Incentivi', 'Post Type General Name', 'madeit' ),
		'singular_name'         => _x( 'Incentivo', 'Post Type Singular Name', 'madeit' ),
		'menu_name'             => __( 'Incentivi', 'madeit' ),
		'name_admin_bar'        => __( 'Incentivo', 'madeit' ),
		'archives'              => __( 'Archivio incentivi', 'madeit' ),
		'all_items'             => __( 'Tutti', 'madeit' ),
		'add_new_item'          => __( 'Aggiungi', 'madeit' ),
		'add_new'               => __( 'Aggiungi', 'madeit' ),
		'new_item'              => __( 'Nuovo', 'madeit' ),
		'edit_item'             => __( 'Modifica', 'madeit' ),
		'update_item'           => __( 'Aggiorna', 'madeit' ),
		'view_item'             => __( 'Visualizza', 'madeit' ),
		'view_items'            => __( 'Visualizza', 'madeit' ),
		'search_items'          => __( 'Cerca', 'madeit' ),
		'not_found'             => __( 'Non trovato', 'madeit' ),
		'not_found_in_trash'    => __( 'Non trovato nel cestino', 'madeit' ),
		'featured_image'        => __( 'Immagine in evidenza', 'madeit' ),
		'set_featured_image'    => __( 'Seleziona immagine in evidenza', 'madeit' ),
		'remove_featured_image' => __( 'Rimuovi immagine in evidenza', 'madeit' ),
		'use_featured_image'    => __( 'Usa immagine in evidenza', 'madeit' ),
		'items_list'            => __( 'Lista incentivi', 'madeit' ),

	);
	$args = array(
		'label'                 => __( 'Incentivo', 'madeit' ),
		'description'           => __( 'Incentivo', 'madeit' ),
		'labels'                => $labels,
		'supports' => array(
			'title',
			'excerpt',
			'thumbnail'
        ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-buddicons-topics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'incentivo', $args );

	// contest
	$labels = array(
		'name'                  => _x( 'Contest', 'Post Type General Name', 'madeit' ),
		'singular_name'         => _x( 'Contest', 'Post Type Singular Name', 'madeit' ),
		'menu_name'             => __( 'Contest', 'madeit' ),
		'name_admin_bar'        => __( 'Contest', 'madeit' ),
		'archives'              => __( 'Archivio contest', 'madeit' ),
		'all_items'             => __( 'Tutti', 'madeit' ),
		'add_new_item'          => __( 'Aggiungi', 'madeit' ),
		'add_new'               => __( 'Aggiungi', 'madeit' ),
		'new_item'              => __( 'Nuovo', 'madeit' ),
		'edit_item'             => __( 'Modifica', 'madeit' ),
		'update_item'           => __( 'Aggiorna', 'madeit' ),
		'view_item'             => __( 'Visualizza', 'madeit' ),
		'view_items'            => __( 'Visualizza', 'madeit' ),
		'search_items'          => __( 'Cerca', 'madeit' ),
		'not_found'             => __( 'Non trovato', 'madeit' ),
		'not_found_in_trash'    => __( 'Non trovato nel cestino', 'madeit' ),
		'featured_image'        => __( 'Immagine in evidenza', 'madeit' ),
		'set_featured_image'    => __( 'Seleziona immagine in evidenza', 'madeit' ),
		'remove_featured_image' => __( 'Rimuovi immagine in evidenza', 'madeit' ),
		'use_featured_image'    => __( 'Usa immagine in evidenza', 'madeit' ),
		'items_list'            => __( 'Lista contest', 'madeit' ),

	);
	$args = array(
		'label'                 => __( 'Contest', 'madeit' ),
		'description'           => __( 'Contest', 'madeit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt'),
		'taxonomies'            => array('argomento', 'partner', 'stato'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'contest', $args );

	// programmi
	$labels = array(
		'name'                  => _x( 'Programmi', 'Post Type General Name', 'madeit' ),
		'singular_name'         => _x( 'Programma', 'Post Type Singular Name', 'madeit' ),
		'menu_name'             => __( 'Programmi', 'madeit' ),
		'name_admin_bar'        => __( 'Programma', 'madeit' ),
		'archives'              => __( 'Archivio programma', 'madeit' ),
		'all_items'             => __( 'Tutti', 'madeit' ),
		'add_new_item'          => __( 'Aggiungi', 'madeit' ),
		'add_new'               => __( 'Aggiungi', 'madeit' ),
		'new_item'              => __( 'Nuovo', 'madeit' ),
		'edit_item'             => __( 'Modifica', 'madeit' ),
		'update_item'           => __( 'Aggiorna', 'madeit' ),
		'view_item'             => __( 'Visualizza', 'madeit' ),
		'view_items'            => __( 'Visualizza', 'madeit' ),
		'search_items'          => __( 'Cerca', 'madeit' ),
		'not_found'             => __( 'Non trovato', 'madeit' ),
		'not_found_in_trash'    => __( 'Non trovato nel cestino', 'madeit' ),
		'featured_image'        => __( 'Immagine in evidenza', 'madeit' ),
		'set_featured_image'    => __( 'Seleziona immagine in evidenza', 'madeit' ),
		'remove_featured_image' => __( 'Rimuovi immagine in evidenza', 'madeit' ),
		'use_featured_image'    => __( 'Usa immagine in evidenza', 'madeit' ),
		'items_list'            => __( 'Lista programmi', 'madeit' ),

	);
	$args = array(
		'label'                 => __( 'Programma', 'madeit' ),
		'description'           => __( 'Programma', 'madeit' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'programma', $args );


}
add_action( 'init', 'madeit_register_post_type', 0 );


// Register Custom Taxonomy
function madeit_register_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Argomenti', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Argomento', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Argomenti', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'argomento', array( 'contest' ), $args );


	$labels = array(
		'name'                       => _x( 'Partner', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Partner', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Partner', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_quick_edit'         => false,
		'meta_box_cb'                => false,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'partner', array( 'contest' ), $args );


	$labels = array(
		'name'                       => _x( 'Obiettivi', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Obiettivo', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Obiettivi', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'obiettivo', array( 'incentivo' ), $args );
	/*
		$labels = array(
			'name'                       => _x( 'Settori', 'Taxonomy General Name', 'madeit' ),
			'singular_name'              => _x( 'Settore', 'Taxonomy Singular Name', 'madeit' ),
			'menu_name'                  => __( 'Settori', 'madeit' ),
			'all_items'                  => __( 'Tutti', 'madeit' ),
			'new_item_name'              => __( 'Nuovo', 'madeit' ),
			'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
			'edit_item'                  => __( 'Modifica', 'madeit' ),
			'update_item'                => __( 'Aggiorna', 'madeit' ),
			'view_item'                  => __( 'Visualizza', 'madeit' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'settore', array( 'incentivo' ), $args );

	*/

	$labels = array(
		'name'                       => _x( 'Destinatari', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Destinatario', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Destinatari', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'destinatario', array( 'incentivo' ), $args );


	$labels = array(
		'name'                       => _x( 'Agevolazioni', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Agevolazione', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Forme di Agevolazione', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'agevolazione', array( 'incentivo' ), $args );

	$labels = array(
		'name'                       => _x( 'Tipologie', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Tipologia', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Tipologie', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipologia', array( 'incentivo' ), $args );

    // tassonomie solo di b/e

	$labels = array(
		'name'                       => _x( 'Ente gestore', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Ente gestore', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Ente gestore', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ente_gestore', array( 'incentivo' ), $args );

	$labels = array(
		'name'                       => _x( 'Ente promotore', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Ente promotore', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Ente promotore', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ente_promotore', array( 'incentivo' ), $args );

	$labels = array(
		'name'                       => _x( 'Territorio', 'Taxonomy General Name', 'madeit' ),
		'singular_name'              => _x( 'Territorio', 'Taxonomy Singular Name', 'madeit' ),
		'menu_name'                  => __( 'Territorio', 'madeit' ),
		'all_items'                  => __( 'Tutti', 'madeit' ),
		'new_item_name'              => __( 'Nuovo', 'madeit' ),
		'add_new_item'               => __( 'Aggiungi nuovo', 'madeit' ),
		'edit_item'                  => __( 'Modifica', 'madeit' ),
		'update_item'                => __( 'Aggiorna', 'madeit' ),
		'view_item'                  => __( 'Visualizza', 'madeit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'territorio', array( 'incentivo' ), $args );

}
add_action( 'init', 'madeit_register_taxonomy', 1 );





/**
 * personalizzazione contest
 *
 */
add_action( 'edit_form_after_title', 'move_excerpt_meta_box' );
function move_excerpt_meta_box( $post ) {
	if ( $post->post_type ==  'contest' ) {
		remove_meta_box( 'postexcerpt', $post->post_type, 'normal' ); ?>
		<h2 style="padding: 20px 0 0;">Sommario</h2>
		<?php post_excerpt_meta_box( $post );?>
		<h2 style="padding: 20px 0 0;">Qual è l'obiettivo</h2>
		<?php
	}else if ( $post->post_type ==  'programma' ) {
		remove_meta_box( 'postexcerpt', $post->post_type, 'normal' ); ?>
        <h2 style="padding: 20px 0 0;">Sommario</h2>
		<?php post_excerpt_meta_box( $post );?>
		<?php
	}
}

add_filter( 'wp_editor_settings', function($settings) {
	$screen = get_current_screen();
	//print_r($screen);
	if($screen->post_type == "contest"){
		$settings['media_buttons']=FALSE;
		$settings['teeny']=TRUE;
	}
	return $settings;

});



// tolgo editor da template che non lo richiedono

add_action( 'admin_init', 'madeit_hide_editor' );

function madeit_hide_editor() {

	if ( isset ( $_GET['post'] ) )
		$post_id = $_GET['post'];
	else if ( isset ( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];

	if( !isset ( $post_id ) || empty ( $post_id ) )
		return;

	$template_file = get_post_meta($post_id, '_wp_page_template', true);

	if($template_file == 'page-templates/page-about.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-manifesto.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-startup.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-ecosistema.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-news.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-partner.php'){
		remove_post_type_support('page', 'editor');
	}

	if($template_file == 'page-templates/page-contatti.php'){
		remove_post_type_support('page', 'editor');
	}
}


// paginazione ad hoc per incentivi e contest
add_filter('pre_get_posts', 'madeid_num_posts_archive');

function madeid_num_posts_archive($query) {
	if (!is_admin() && $query->is_post_type_archive('incentivo') && $query->is_main_query()) {
		$query->set('posts_per_page', -1);
		$query->set('orderby', 'title');
		$query->set('order', 'ASC');
	}


	// inutile nel caso di archivio con ordinamento custom
	if (!is_admin() && $query->is_post_type_archive('contest') && $query->is_main_query()) {
		$query->set('posts_per_page', -1);
		$query->set('orderby', 'meta_value');
		$query->set('meta_key', 'data_inizio_contest');
		$query->set('order', 'ASC');
	}
	return $query;
}
