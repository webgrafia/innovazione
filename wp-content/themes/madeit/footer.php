<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ministero
 */
?>

<footer id="site-footer">
    <div id="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-10 col-md-6 col-lg-2 mb-3 mb-lf-0">
                    <div class="brand-footer-wrap">
                        <img src="<?php bloginfo("template_url"); ?>/assets/img/logo-or.svg" class="img-fluid" alt="MadeIT" />
                    </div>
                </div>
                <div class="col-12 col-md-5 offset-md-1 col-lg-3 offset-lg-0 mb-5 mb-lg-0">
                    <p><strong><?php _e("Aderisci al manifesto e rimani aggiornato", "madeit"); ?></strong></p>
                    <a class="button invert" href="<?php echo get_field("link_manifesto", "options"); ?>" target="_blank"><?php _e("Aderisci", "madeit"); ?></a>
                </div>
                <div class="col-6 col-md-3 col-lg-2  mb-5 mb-md-0 pr-0">
                    <nav class="menu-madeit-container" aria-label="MadeIT">
	                <?php
	                wp_nav_menu( array( 'theme_location' => 'footer-1-menu', 'container' => '', 'menu_class' => 'nav flex-column','items_wrap' => '<ul id="%1$s" class="%2$s"><li class="nav-item"><a class="nav-link">'.__("MadeIT", "madeit").'</a></li>%3$s</ul>', 'walker' => new madeit_walker()) );
	                ?>
                    </nav>
                </div>

                <div class="col-6 col-md-2 col-lg-1">
                    <nav class="menu-link-utili-container" aria-label="Link">
	                <?php
	                wp_nav_menu( array( 'theme_location' => 'footer-2-menu', 'container' => '', 'menu_class' => 'nav flex-column','items_wrap' => '<ul id="%1$s" class="%2$s"><li class="nav-item"><a class="nav-link">'.__("Link Utili", "madeit").'</a></li>%3$s</ul>', 'walker' => new madeit_walker()) );
	                ?>
                    </nav>
                </div>

                <div class="col-6 col-md-4 col-lg-2">
                    <strong><?php _e("Contatti", "madeit"); ?></strong><br />
	                <?php  echo get_field("contatti", "options"); ?>
                </div>

                <div class="col-6 col-md-4 col-lg-2 pr-0">
                    <strong><?php echo get_field("testo_social", "options"); ?></strong>
                    <nav class="pt-2" aria-label="MID">
                        <ul class="nav nav-social">
	                        <?php  get_template_part("template-parts/common/social");  ?>
                        </ul>
                    </nav>
                    <?php
                    /*
                    <ul class="lang">
                        <li class="current">
                            <a class="lang-item">ITA</a>
                            <ul class="submenu">
                                <li class="lang-item lang-item-2 lang-item-it current-lang lang-item-first">
                                    <a hreflang="it-IT" href="http://178.62.6.197/innovazione/madeit/" class="" lang="it-IT">ITA</a>
                                </li>
                                <li class="lang-item lang-item-5 lang-item-en">
                                    <a hreflang="en-GB" href="http://178.62.6.197/innovazione/madeit/en/home-2/" lang="en-GB">EN</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    */
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div id="colophon">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 text-center">
                    <?php  echo get_field("footer_text", "options"); ?>
                    <nav class="legal-nav" aria-label="footer menu">
	                <?php
	                wp_nav_menu( array( 'theme_location' => 'subfooter-menu', 'container' => '', 'menu_class' => 'nav justify-content-center', 'walker' => new madeit_walker()) );
	                ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>

<script>window.__PUBLIC_PATH__ = '<?php echo get_template_directory_uri(); ?>/vendors/bootstrap-italia/fonts'</script>

<?php wp_footer(); ?>

</body>
</html>
