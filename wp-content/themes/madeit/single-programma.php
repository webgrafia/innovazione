<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				$id_programma = $post->ID;
				?>

                <section class="page-hero" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-lg-12 mb-5">
                                <div class="program-ico <?php echo get_field("colore_programma", $post); ?>">
                                    <h1 class="program-name" style="background-image:url('<?php  the_field("logo_programma"); ?>');">
                                        MadeIT
                                        <strong><?php echo get_field("nome_programma", $post); ?></strong>
                                    </h1>
                                </div>
                            </div>


                            <div class="col-12 col-lg-8 col-xl-6 pr-xl-5">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><?php _e("MadeIT per le startup", "madeit"); ?></li>
                                        <li class="breadcrumb-item active" aria-current="page"><?php _e("Programma di eccellenza", "madeit"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <div class="text">
                                    <p><?php echo get_the_excerpt(); ?></p>
                                </div>

								<?php // if(get_field("link_candidatura")){ ?>
                                    <div class="reference reference-mb-fix d-md-none">
										<?php if(get_field("link_candidatura")){ ?>
                                            <div class="button-deadline">
                                                <a class="link-wrap" href="<?php the_field("link_candidatura"); ?>"><?php _e("Candidati", "madeit"); ?></a>
                                                <div class="deadline-wrap">
                                                    <strong><?php the_field("label_inizio_fine"); ?></strong><br />
													<?php the_field("value_inizio_fine"); ?>
                                                </div>
                                            </div>
										<?php }else{ ?>
                                            <!-- variante slim non attivo -->
                                            <div class="button-deadline disabled">
                                                <a class="link-wrap"><?php _e("Candidati", "madeit"); ?></a>
                                                <div class="deadline-wrap">
                                                    <strong><?php the_field("label_inizio_fine"); ?></strong><br />
													<?php the_field("value_inizio_fine"); ?>
                                                </div>
                                            </div>
										<?php } ?>
                                    </div>
								<?php // } ?>

                            </div>
                        </div>
                    </div>
                </section>




                <section>
                    <div class="sidebar-wrap sidebar-fix">
                        <div class="container-fluid container-sidebar d-none d-md-block">
                            <div class="row justify-content-end">
                                <div class="col col-sidebar">
                                    <div class="pin-content pt-large pb-small">
                                        <div class="sidebar-index-wrap single-counter mb-5">
                                            <strong class="title"><?php _e("Programma di eccellenza", "madeit"); ?></strong>
                                            <ol class="index">
                                                <li><a href="#item-1"><?php the_field("titolo_sezione_1"); ?></a></li>
                                                <li><a href="#item-2"><?php the_field("titolo_sezione_2"); ?></a></li>
                                                <li><a href="#item-3"><?php the_field("titolo_sezione_3"); ?></a></li>
                                                <?php
                                                $loghi = get_field("loghi");
                                                if(isset($loghi[0]["logo"]["url"])){ ?>
                                                <li><a href="#item-4"><?php the_field("titolo_sezione_4"); ?></a></li>
	                                            <?php } ?>
                                            </ol>
                                        </div>
										<?php // if(get_field("link_candidatura")){ ?>
											<?php if(get_field("link_candidatura")){ ?>
                                                <div class="footer-wrap w-75">
                                                    <a href="<?php the_field("link_candidatura"); ?>" class="button full mb-3" target="_blank"><?php _e("Candidati al programma", "madeit"); ?></a>
                                                    <p class="text-center">
                                                        <strong><?php the_field("label_inizio_fine"); ?></strong><br />
														<?php the_field("value_inizio_fine"); ?>
                                                    </p>
                                                </div>
											<?php }else{ ?>
                                                <div class="footer-wrap w-75">
                                                    <a  class="button disabled full mb-3" target="_blank"><?php _e("Candidati al programma", "madeit"); ?></a>
                                                    <p class="text-center">
                                                        <strong><?php the_field("label_inizio_fine"); ?></strong><br />
														<?php the_field("value_inizio_fine"); ?>
                                                    </p>
                                                </div>
											<?php } ?>
										<?php // } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar-main program-main">
                            <div class="square-title section-title-counter section-<?php echo get_field("colore_programma", $post); ?>">
                                <!-- punto 1 -->
                                <div class="item-wrap scroll-item-wrap pt-xxlarge  pb-xlarge-fix">

                                    <a class="bm-nospace" id="item-1" name="item-1"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">

                                                <div class="item">
                                                    <div class="item-title-wrap">
                                                        <h3 class="item-title">
                                                            <span><?php the_field("titolo_sezione_1"); ?></span>
                                                    </div>
                                                    <div class="text">
														<?php the_content(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fine punto 1 -->

                                <!-- punto 2 -->
                                <div class="item-wrap scroll-item-wrap pb-xxlarge-fix">

                                    <a class="bm-nospace" id="item-2" name="item-2"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">
                                                <div class="item-title-wrap">
                                                    <h3 class="item-title">
                                                        <span><?php the_field("titolo_sezione_2"); ?></span>
                                                    </h3>
                                                </div>
                                                <div class="text">
													<?php the_field("testo_sezione_2"); ?>
                                                </div>




                                                <div class="row min-card-container mt-xxsmall">

													<?php
													$caratteristiche = get_field("caratteristiche");
													foreach ($caratteristiche as $item){
														?>
                                                        <div class="col-6 col-lg-4">
                                                            <div class="mini-card">
                                                                <div class="img-wrap">
                                                                    <img src="<?php echo $item["logo"]; ?>" alt="<?php echo esc_attr($item["titolo"]); ?>" class="img-fluid" />
                                                                </div>
                                                                <h4 class="title"><?php echo $item["titolo"]; ?></h4>
                                                                <div class="text">
                                                                    <p><?php echo $item["testo"]; ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
														<?php
													}
													?>
                                                </div>
                                            </div><!-- colonna principale -->
											<?php if(get_field("file_timeline")){ ?>
                                                <div class="col-12 col-md-8 offset-md-1 col-lg-5 offset-lg-4 col-xl-4 offset-xl-5">
                                                    <div class="card-download">
                                                        <a class="download-ico" href="#">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-pdf.svg" alt="pdf" class="img-fluid" />
                                                        </a>
                                                        <h5 class="title"><?php _e("Scarica la Timeline del programma", "madeit"); ?></h5>

                                                        <div class="text-right">
                                                            <a class="button-link button-download" href="<?php the_field("file_timeline"); ?>" download="Timeline programma"><span><?php _e("Scarica subito", "madeit"); ?></span></a>
                                                        </div>

                                                    </div>
                                                </div>
											<?php } ?>
                                        </div><!-- /row -->
                                    </div><!-- /container -->
                                </div>   <!-- /item-wrap -->
                                <!-- fine punto 2 -->

                                <!-- punto 3 -->
                                <div class="item-wrap scroll-item-wrap dark-wrap pt-xxlarge pb-xxlarge-fix">

                                    <a class="bm-nospace" id="item-3" name="item-3"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">
                                                <div class="item-title-wrap">
                                                    <h3 class="item-title">
                                                        <span><?php the_field("titolo_sezione_3"); ?></span>
                                                    </h3>
                                                </div>
                                                <div class="program-ico <?php echo get_field("colore_programma", $post); ?> mt-large mb-medium">
                                                    <h1 class="program-name" style="background-image:url('<?php  the_field("logo_programma"); ?>');">
                                                        MadeIT
                                                        <strong><?php echo get_field("nome_programma", $post); ?></strong>
                                                    </h1>
                                                </div>
                                                <div class="text">
													<?php the_field("testo_sezione_3"); ?>
                                                </div>
                                            </div>
											<?php if(get_field("file_regolamento")){ ?>
                                                <div class="col-12 col-md-8 offset-md-1 col-lg-5 offset-lg-4 col-xl-4 offset-xl-5 mt-5">
                                                    <div class="card-download">
                                                        <a class="download-ico" href="#">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-pdf.svg" alt="pdf" class="img-fluid" />
                                                        </a>
                                                        <h5 class="title mb-4 mb-md-0"><?php _e("Leggi il regolamento", "madeit"); ?></h5>
                                                        <div class="text-right mt-4">
                                                            <a class="button-link button-download" href="<?php the_field("file_regolamento"); ?>" download="Regolamento programma"><span><?php _e("Scarica subito", "madeit"); ?></span></a>
                                                        </div>
                                                    </div>
                                                </div>
											<?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- fine punto 3 -->

                                <?php
                                $loghi = get_field("loghi");
                                if(isset($loghi[0]["logo"]["url"])){
                                ?>
                                <!-- punto 4 -->
                                <div class="item-wrap scroll-item-wrap  pt-xxlarge pb-xxlarge">

                                    <a class="bm-nospace" id="item-4" name="item-4"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">


                                                <div class="item-title-wrap">
                                                    <h3 class="item-title">
                                                        <span><?php the_field("titolo_sezione_4"); ?></span>
                                                    </h3>
                                                </div>

                                                <div class="row logo-card-container mt-xlarge wrap-xxsmall">
													<?php
													foreach ($loghi as $item){
														?>
                                                        <div class="col-6 col-sm-4 col-lg-3">
                                                            <div class="card-logo-square-light">
																<?php if($item["link"] != ""){ ?>
                                                                    <a href="<?php echo $item["link"]; ?>" target="_blank" class="logo-wrap" style="background-image:url('<?php echo $item["logo"]["url"]; ?>');"></a>
																<?php }else{ ?>
                                                                    <span class="logo-wrap" style="background-image:url('<?php echo $item["logo"]["url"]; ?>');"></span>
																<?php } ?>
                                                            </div>
                                                        </div>
														<?php
													}
													?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fine punto 4 -->
                                <?php
                                }
                                ?>
                            </div><!-- /square-sections -->
                        </div>
                    </div>
                </section>

                <?php
				$related = get_posts(
					array(
						'post_type' => "programma",
						'numberposts'  => 2,
						'post__not_in' => array( $id_programma ),
						'order' => "ASC"
					)
				);

				$realactive = 0;
				if( $related ) {
					foreach ( $related as $post ) {
                        if(get_field("programma_attivo", $post)){
                            $realactive++;
                        }
					}
				}

				if($realactive){
                ?>

                <section class="pt-large pb-xxlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="section-heading">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php _e("Guarda gli altri Programmi che potrebbero interessarti", "madeit"); ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row card-wrap wrap-xxsmall">

							<?php

							if( $related ) {
								foreach( $related as $post ) {
									setup_postdata($post);
									if(get_field("programma_attivo", $post)){
									?>
                                    <div class="col-12 col-md-6 col-lg-4 order-lg-1">
										<?php get_template_part( 'template-parts/card/programma'); ?>
                                    </div>
									<?php
                                    }
								}
								wp_reset_postdata();
							}
							?>

                            <div class="col-12 col-lg-4 d-flex align-items-center order-lg-0">
                                <a class="button-big-link column" href="<?php
								$startup = get_page_by_template("page-templates/page-startup.php");
								echo get_permalink($startup);
								?>">
                                    <span class="text"> <?php _e("Torna alla sezione dei <br class=\"d-none d-sm-block\" /> programmi", "madeit"); ?> </span>
                                    <br />
                                    <span class="arrow back"></span>
                                </a>
                            </div>

                        </div>
                    </div>
                </section>

                    <?php
                }

			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div><!-- /.page-content -->


<?php get_footer();
