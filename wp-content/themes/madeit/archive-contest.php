<?php
/**
 * The archive incentivi template file
 */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */

			?>

            <section class="page-hero page-hero--large plus-filter4" style="background-image:url('<?php bloginfo("template_url"); ?>//media/img/hero-contest.jpg');">
                <div class="container hero-caption">
                    <div class="row">

                        <div class="col-12 col-lg-8 col-xl-6 pr-xl-5">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><?php _e("MadeIT per le startup", "madeit"); ?></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php _e("Tutti i tech contest", "madeit"); ?></li>
                                </ol>
                            </nav>
                            <h2 class="title">
								<?php _e("Le attività di open innovation ed i tech contest in Italia", "madeit"); ?>
                            </h2>
                            <div class="text">
                                <p><?php _e("Scopri le attività di open innovation e i tech contest organizzati in Italia. Partecipa e contribuisci anche tu a potenziare l'ecosistema tech italiano.", "madeit"); ?></p>
                            </div>
                        </div>
                    </div>

                </div>
            </section>



            <section>
                <div class="light-wrap-offset offset-on-hero pb-doublexlarge">
                    <div class="container filter-wrap">
                        <div class="row">

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per argomenti", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti gli argomenti", "madeit"); ?>" id="select_argomento">
                                        <option value=""><?php _e("Tutti gli argomenti", "madeit"); ?></option>
										<?php
										$argomenti = get_terms(array(
											'taxonomy' => 'argomento',
											'hide_empty' => true,
										));
										foreach ( $argomenti as $argomento ) { ?>
                                            <option value="<?php echo $argomento->slug; ?>"><?php echo $argomento->name; ?></option>
											<?php
										}
										?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per organizzatore", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti gli organizzatori", "madeit"); ?>" id="select_partner">
                                        <option value=""><?php _e("Tutti gli organizzatori", "madeit"); ?></option>
										<?php
										$partners = get_terms(array(
											'taxonomy' => 'partner',
											'hide_empty' => true,
										));
										foreach ( $partners as $partner ) { ?>
                                            <option value="<?php echo $partner->slug; ?>"><?php echo $partner->name; ?></option>
											<?php
										}
										?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per stato del contest", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti i contest", "madeit"); ?>" id="select_status">
                                        <option value=""><?php _e("Tutti i contest", "madeit"); ?></option>
                                        <option value="pre-iscrizione"><?php _e("Iscrizione attiva a breve ", "madeit"); ?></option>
                                        <option value="progress-iscrizione"><?php _e("Iscrizione in corso", "madeit"); ?></option>
                                        <!--
                                        <option value="post-iscrizione"><?php _e("Iscrizione conclusa", "madeit"); ?></option>
                                        <option value="pre-contest"><?php _e("Contest ancora non iniziato", "madeit"); ?></option>
                                        //-->
                                        <option value="progress-contest"><?php _e("Contest in corso", "madeit"); ?></option>
                                        <!-- <option value="post-contest"><?php _e("Contest concluso", "madeit"); ?></option> //-->
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="container">
                        <div class="row contest-row card-wrap wrap-xxsmall" id="grid">
							<?php
							global $post;
							$origpost = $post;
							$contests = madeit_list_contests();
							foreach ( $contests as $post ) {
								setup_postdata($post);
								$datagroups = array();
								$argomenti = wp_get_object_terms($post->ID, "argomento");
								if($argomenti){
									foreach ($argomenti as $argomento){
										$datagroups[]= $argomento->slug;
									}
								}
								$partners = wp_get_object_terms($post->ID, "partner");
								if($partners){
									foreach ($partners as $partner){
										$datagroups[]= $partner->slug;
									}
								}
								$statuses = madeit_contest_status($post);
								foreach ($statuses as $status)
									$datagroups[] = $status;


								?>
                                <div class="col-12 col-md-6 col-lg-4 card-filtro" data-groups='[<?php
                                if ( function_exists( 'pll_current_language' ) ) echo '"'.pll_current_language().'",';
                                ?>"<?php echo implode('","',$datagroups); ?>"]'>
									<?php
									get_template_part( 'template-parts/card/contest' );
									?>
                                </div>
								<?php
							}
							$post = $origpost;
							wp_reset_postdata();

							?>

                        </div>
                        <div class="col-12 text-wrap text-center site-error-message pt-doublelarge" id="noresults" style="display: none;">
                            <div class="text">
                                <img class="img-responsive mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/noresult.png">
                                <br>
                                <p>Ops, sembra che i filtri che hai realizzato non diano risultati…</p>
                                <a class="button" href="javascript:resetMyFilters();">Azzera filtri</a>
                            </div>
                        </div>

                    </div>

                </div>
            </section>


<?php
        /*

            <section class="my-5 my-lg-0 py-5 py-lg-0">
                <div class="container">
                    <div class="txtimg row">
                        <div class="col-12 col-lg-6 mb-4 mb-5 mb-lg-0">
                            <div class="img-wrap offset-container-left-lg">
                                <img class="img-fluid" src="<?php the_field("immagine_cta_partner", "options"); ?>" alt="" />
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            <div class="hgroup">
                                <strong class="supertitle"><?php the_field("occhiello_cta_partner", "options"); ?></strong>
                                <h2 class="title"><?php the_field("titolo_cta_partner", "options"); ?></h2>
                            </div>
                            <div class="text">
                                <p><?php the_field("testo_cta_partner", "options"); ?></p>
                            </div>
                            <div class="reference">
                                <a class="button large mr-5" target="_blank" href="<?php the_field("link_partner", "options"); ?>"><?php the_field("testo_bottone_cta_partner", "options"); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

*/ ?>

		<?php
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>

<?php get_footer();
