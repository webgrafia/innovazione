<?php
/**
 * The archive incentivi template file
 */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */

			?>

            <section class="page-hero page-hero--large plus-filter4">
                <div class="container hero-caption">
                    <div class="row">

                        <div class="col-12 col-lg-8 col-xl-6 pr-xl-5">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><?php _e("MadeIT per le startup", "madeit"); ?></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php _e("Tutti gli incentivi", "madeit"); ?></li>
                                </ol>
                            </nav>
                            <h2 class="title"><?php _e("Gli strumenti e gli incentivi per le startup e le PMI innovative", "madeit"); ?></h2>
                            <div class="text">
                                <p><?php _e("Scopri i più importanti incentivi e le agevolazioni per la nascita, la crescita e l’internazionalizzazione delle <a href='https://www.mise.gov.it/index.php/it/impresa/competitivita-e-nuove-imprese/start-up-innovative#requisiti' target='_blank'>startup innovative</a> e delle <a href='https://www.mise.gov.it/index.php/it/impresa/piccole-e-medie-imprese/pmi-innovative' target='_blank'>PMI innovative</a> italiane", "madeit"); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>





            <section>
                <div class="light-wrap-offset-down responsive-nobg">
                    <div class="container filter-wrap">
                        <div class="row">

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per destinatario", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti i destinatari", "madeit"); ?>">
                                        <option value=""><?php _e("Tutti i destinatari", "madeit"); ?></option>
				                        <?php
				                        $destinatari  = get_terms(array(
					                        'taxonomy' => 'destinatario',
					                        'hide_empty' => true,
				                        ));
				                        foreach ( $destinatari as $destinatario ) { ?>
                                            <option value="<?php echo $destinatario->slug; ?>"><?php echo $destinatario->name; ?></option>
					                        <?php
				                        }
				                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per obiettivo", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti gli obiettivi", "madeit"); ?>">
                                        <option value=""><?php _e("Tutti gli obiettivi", "madeit"); ?></option>
				                        <?php
				                        $obiettivi  = get_terms(array(
					                        'taxonomy' => 'obiettivo',
					                        'hide_empty' => true,
				                        ));
				                        foreach ( $obiettivi as $obiettivo ) { ?>
                                            <option value="<?php echo $obiettivo->slug; ?>"><?php echo $obiettivo->name; ?></option>
					                        <?php
				                        }
				                        ?>
                                    </select>
                                </div>
                            </div>

                            <?php
  /*
                            ?>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per settore", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti i settori", "madeit"); ?>">
                                        <option value=""><?php _e("Tutti i settori", "madeit"); ?></option>
										<?php
										$settori  = get_terms(array(
											'taxonomy' => 'settore',
											'hide_empty' => true,
										));
										foreach ( $settori as $settore ) { ?>
                                            <option value="<?php echo $settore->slug; ?>"><?php echo $settore->name; ?></option>
											<?php
										}
										?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per ente gestore", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti gli enti", "madeit"); ?>">
                                        <option value=""><?php _e("Tutti gli enti", "madeit"); ?></option>
				                        <?php
				                        $enti_g  = get_terms(array(
					                        'taxonomy' => 'ente_gestore',
					                        'hide_empty' => true,
				                        ));
				                        foreach ( $enti_g as $ente_g ) { ?>
                                            <option value="<?php echo $ente_g->slug; ?>"><?php echo $ente_g->name; ?></option>
					                        <?php
				                        }
				                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per ente promotore", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutti gli enti", "madeit"); ?>">
                                        <option value=""><?php _e("Tutti gli enti", "madeit"); ?></option>
				                        <?php
				                        $enti_p  = get_terms(array(
					                        'taxonomy' => 'ente_promotore',
					                        'hide_empty' => true,
				                        ));
				                        foreach ( $enti_p as $ente_p ) { ?>
                                            <option value="<?php echo $ente_p->slug; ?>"><?php echo $ente_p->name; ?></option>
					                        <?php
				                        }
				                        ?>
                                    </select>
                                </div>
                            </div>
<?php
*/
                            ?>


                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="filter">
                                    <label><?php _e("Filtra per tipologia di supporto", "madeit"); ?></label>
                                    <select class="mi-select" title="<?php _e("Tutte le tipologie", "madeit"); ?>">
                                        <option value=""><?php _e("Tutte le tipologie", "madeit"); ?></option>
				                        <?php
				                        $agevolazioni  = get_terms(array(
					                        'taxonomy' => 'agevolazione',
					                        'hide_empty' => true,
				                        ));
				                        foreach ( $agevolazioni as $agevolazione ) { ?>
                                            <option value="<?php echo $agevolazione->slug; ?>"><?php echo $agevolazione->name; ?></option>
					                        <?php
				                        }
				                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-12 col-md-6 col-lg-3">
                                <span class="text-white">Filtra per emergenza sanitaria</span>
                                <div class="filter toggles pt-2">
                                    <label class="text-uppercase text-center"><?php _e("Misure Covid 19", "madeit"); ?>
                                        <input type="checkbox" id="covid" value="covid">
                                        <span class="lever leverRight"></span>
                                    </label>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="container">
                        <div class="row incentivi-row card-wrap wrap-xxsmall" id="grid">

							<?php
							while ( have_posts() ) : the_post();
								setup_postdata($post);
								$datagroups = array();
								$obiettivi = wp_get_object_terms($post->ID, "obiettivo");
								if($obiettivi){
									foreach ($obiettivi as $obiettivo){
										$datagroups[]= $obiettivo->slug;
									}
								}
								/*
								$settori = wp_get_object_terms($post->ID, "settore");
								if($settori){
									foreach ($settori as $settore){
										$datagroups[]= $settore->slug;
									}
								}
								$enti_g = wp_get_object_terms($post->ID, "ente_gestore");
								if($enti_g){
									foreach ($enti_g as $ente_g){
										$datagroups[]= $ente_g->slug;
									}
								}
								$enti_p = wp_get_object_terms($post->ID, "ente_promotore");
								if($enti_p){
									foreach ($enti_p as $ente_p){
										$datagroups[]= $ente_p->slug;
									}
								}
								*/
								$destinatari = wp_get_object_terms($post->ID, "destinatario");
								if($destinatari){
									foreach ($destinatari as $destinatario){
										$datagroups[]= $destinatario->slug;
									}
								}
								$agevolazioni = wp_get_object_terms($post->ID, "agevolazione");
								if($agevolazioni){
									foreach ($agevolazioni as $agevolazione){
										$datagroups[]= $agevolazione->slug;
									}
								}
								$tipologie = wp_get_object_terms($post->ID, "tipologia");
								if($tipologie){
									foreach ($tipologie as $tipologia){
										$datagroups[]= $tipologia->slug;
									}
								}
								?>
                                <div class="col-12 col-md-6 col-lg-4 card-filtro" data-groups='[<?php
								if ( function_exists( 'pll_current_language' ) ) echo '"'.pll_current_language().'",';
									?>"<?php echo implode('","',$datagroups); ?>"]'>
                                    <?php
                                    get_template_part( 'template-parts/card/incentivo' );
                                    ?>
                                </div>
							<?php endwhile; ?>
                        </div>
                        <div class="col-12 text-wrap text-center site-error-message pt-doublelarge pb-doublelarge" id="noresults" style="display: none;">
                            <div class="text">
                                <img class="img-responsive mw-100" src="<?php echo get_template_directory_uri(); ?>/assets/img/noresult.png">
                                <br>
                                <p>Ops, sembra che i filtri che hai realizzato non diano risultati…</p>
                                <a class="button" href="javascript:resetMyFilters();">Azzera filtri</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

		<?php
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>

<?php get_footer();
