<?php get_header(); ?>

<div id="page-content" class="main no-hero">

	<div class="container pt-xxlarge pb-doublexlarge">
		<div class="row site-error-message">
			<div class="col-12 col-lg-8 offset-lg-2">
				<div class="img-wrap">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/page_404.svg" class="img-fluid" alt="Error 404" />
				</div>
			</div>

			<div class="text-wrap col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3 text-center">
				<div class="text">
					<p>Ops, sembra che la pagina che stai cercando non esista, o forse potresti aver digitato male qualcosa</p>
					<a class="button" href="<?php echo home_url(); ?>">Torna alla Home</a>
				</div>
			</div>

		</div>
	</div>

</div>

<?php get_footer();

