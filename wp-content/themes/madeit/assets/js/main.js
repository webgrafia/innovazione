var controller = new ScrollMagic.Controller();

function menuControl() {
  var menuTop = $('#site-header').height();
  $('.bm-nospace').css('top', (menuTop + 30) * -1);
  if ($('#page-submenu').length < 1) {  
    $('.bm').css('top', menuTop * -1);
  }
  else {
    var submenuTop = $('#page-submenu').height();
    $('.bm').css('top', (menuTop + submenuTop) * -1);
  }
  if($(window).width() < 992) {
    var wrapH = $(window).height() - menuTop;
    $('#main-menu').css('top', menuTop);
    $('#main-menu .navs-wrap').css('height', wrapH);
    $('#main-menu').on('show.bs.collapse', function () {    
      $('body').addClass('no-scroll');
    })
    $('#main-menu').on('hide.bs.collapse', function () {
      $('body').removeClass('no-scroll');
    })
  }
  else {
    $('#main-menu').css('top', '');
    $('body').removeClass('no-scroll');
    $('#main-menu .navs-wrap').css('height', '');
  }
  if($('.no-hero').length > 0) {
    $('#site-header').addClass('permanent-sticky');
  }
}

function fixMenu() {
  var headH = $('#site-header').outerHeight();
  var scene = new ScrollMagic.Scene({
    triggerHook: 'onLeave',
    offset:headH,
    triggerElement: "#page-content",
    duration: 0}).setClassToggle("#site-header","sticky").addTo(controller);

  if($('#page-submenu').length > 0) {
    var heroOffset = $('.submenu-control').height();
    var scene = new ScrollMagic.Scene({
      triggerHook: 'onLeave',
      offset:heroOffset - headH,
      triggerElement: ".submenu-control",
      duration: 0}).setPin("#page-submenu").addTo(controller);
  }
}

function fixSidebar() {
  var headH = $('#site-header').outerHeight();
  var pinH = $('.pin-content').outerHeight();
  var mainH = $('.sidebar-main').outerHeight();
  var pinDuration = $('.sidebar-wrap').outerHeight() - headH - $('.pin-content').outerHeight();
  if (pinH < mainH) {
    var scene = new ScrollMagic.Scene({
      triggerElement: ".sidebar-fix",
      triggerHook: 'onLeave',
      offset:headH*-1,
      reverse: true,
      duration:pinDuration
    }).setPin(".pin-content").addTo(controller);
  }
}

function scrollMenuWatch(item,menuItem) {
  //console.log('scroll');
  var listItem = document.querySelectorAll(item);
  var indexItem = document.querySelectorAll(menuItem);
  $(item).each(function (i, value) {
    var duration = $(listItem[i]).outerHeight() + 100;
    var sceneMenuWatch = new ScrollMagic.Scene({
      triggerElement: listItem[i],
      triggerHook: 'onProgress',
      offset:100,
      duration:duration,
      reverse: true 
    }).setClassToggle(indexItem[i], "active").addTo(controller);
  });
}

function sceneIn(element) {
  $(element).each(function (i, value) {
   // var duration = $(listItem[i]).outerHeight() + 100;
    var sceneIn = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
    //  offset:100,
      //duration:duration,
      reverse: false 
    }).setClassToggle(this, "view-on").addTo(controller);
  });
  
}


$(document).ready(function($){
  $('.mi-select').selectpicker({
    //mobile:true;
    //noneSelectedText:"test",
  });
  fixMenu();
  menuControl();
  if($('.sidebar-fix').length > 0){  
    fixSidebar();
  }
  if($('.bm-menu').length > 0){  
    scrollMenuWatch('.bm-section','#page-submenu .page-submenu > li');
  }
  if($('.scroll-list').length > 0){  
    scrollMenuWatch('.scroll-list > li','.index > li');
  }
  if($('.scroll-item-wrap').length > 0){  
    scrollMenuWatch('.scroll-item-wrap','.index > li');
  }
  sceneIn('.txtimg');
  sceneIn('.hero-footer .button-down');
  
  /* scroll animato sui link a bookmark in pagina */
  $('a[href^="#"]').not('.tab-control').on('click keypress', function(event) {
    var target = $( $(this).attr('href') );
    if( target.length ) {
      event.preventDefault();
      $('html, body').animate({
	scrollTop: target.offset().top
      }, 500);
    }
  });

  $('.hero-footer .button-down').on('mouseover',function() {
    $(this).removeClass('view-on');
  });
  /****** SLIDERS *******/
  $('#home-partner-sl').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:true,
    touchDrag:true,
    responsive:{
      0:{
	stagePadding:30,
        items:2,
      },
      576:{
        items:3
      },
      992: {
	items:4
      },
      1200:{
        items:6
      }
    }
  });

  $('#tech-partner').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:true,
    touchDrag:true,
    responsive:{
      0:{
        stagePadding:20,
        items:2,
      },
      576:{
        items:3
      },
      992: {
        items:4
      },
      1200:{
        items:6
      }
    }
  });


  $('#program-partner').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:true,
    touchDrag:true,
    responsive:{
      0:{
        stagePadding:20,
        items:2,
      },
      576:{
        items:3
      },
      992: {
        items:4
      },
      1200:{
        items:6
      }
    }
  });

  $('#supporter-partner').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:true,
    touchDrag:true,
    responsive:{
      0:{
        stagePadding:20,
        items:2,
      },
      576:{
        items:3
      },
      992: {
        items:4
      },
      1200:{
        items:6
      }
    }
  });

  $('#home-news-sl').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:true,
    stagePadding:30,
    touchDrag:true,
    responsive:{
      0:{
	items:1
      },
      768:{
	items:2
      },
      992:{
	items:3
      }
    }
  });

  $(window).on('resize',function() {
    menuControl();
    controller.destroy(true);
    controller = new ScrollMagic.Controller();
    fixMenu();
    if($('.sidebar-fix').length > 0){  
      fixSidebar();
    }
    if($('.bm-menu').length > 0){  
      scrollMenuWatch('.bm-section','#page-submenu .page-submenu > li');
    }
    if($('.scroll-list').length > 0){  
	scrollMenuWatch('.scroll-list > li','.index > li');
    }
    if($('.scroll-item-wrap').length > 0){  
      scrollMenuWatch('.scroll-item-wrap','.index > li');
    }
    sceneIn('.txtimg');
  });

});
