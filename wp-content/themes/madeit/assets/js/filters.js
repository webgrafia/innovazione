
/**
 * Shufflejs per filtri contest
 **/


var Shuffle = window.Shuffle;
var element = document.getElementById('grid');

var shuffle = new Shuffle(element, {
    itemSelector: '.card-filtro',
    filterMode: Shuffle.FilterMode.ALL,
    buffer: 1,
});

var SearchKey = [];

var  SelectFilters = document.getElementsByTagName('select');
var inputList = Array.prototype.slice.call(SelectFilters);
inputList.forEach(addMyListener);

const covid =  document.getElementById('covid');
addMyListener(covid);

function addMyListener(item) {
    item.addEventListener('change', function() {
        applyFilter();
    });
}


function applyFilter    (value) {

    var inputs;
    var all = [];
    selects = document.getElementsByTagName('select');

    inputs = Array.prototype.slice.call(selects);
    var matches= inputs.filter(function(input) {
        if(input.value)
            all.push(input.value)
        SearchKey = all;
    });

    console.log(all);

    var covid =  document.getElementById('covid');

    console.log(covid.checked);

    if(covid.checked == true){
        all.push("covid");
        SearchKey = all;
    }
    console.log(all);


    if(SearchKey.length)
        shuffle.filter(SearchKey);
    else
        shuffle.filter(Shuffle.ALL_ITEMS);

    if(shuffle.visibleItems == 0)
        document.getElementById("noresults").style.display = "block";
    else
        document.getElementById("noresults").style.display = "none";

}

function resetMyFilters(){
    shuffle.filter(Shuffle.ALL_ITEMS);
    document.getElementById("noresults").style.display = "none";

    $(".mi-select").val('default');
    $(".mi-select").selectpicker("refresh");

}