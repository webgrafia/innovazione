<?php get_header(); ?>

    <div id="page-content" class="main no-hero" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>

                <div class="container pt-xlarge pb-xxsmall">
                    <div class="row">

                        <div class="col-12 col-lg-10 col-xl-8">
                            <div class="section-heading top-page">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <h3 class="subtitle"><?php the_field("sommario"); ?></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="light-wrap-offset offset-small pb-xxlarge">
                    <div class="container">
                        <div class="row cards-v-wrap">

                            <div class="col-12 col-md-10 offset-md-1 mb-xxsmall">
                                <div class="card-content-item red">
									<?php the_content(); ?>
                                </div>
                            </div>

							<?php
							$blocchi = get_field("blocchi");
							if($blocchi){
								foreach ($blocchi as $blocco){
									?>
                                    <div class="col-12 col-md-10 offset-md-1 mb-xxsmall">
                                        <div class="card-content-item red">
											<?php echo wpautop($blocco["blocco"]); ?>
											<?php
											if($blocco["downloads"]){
												foreach ($blocco["downloads"] as $download){
													?>
                                                    <div class="footer-wrap text-right">
                                                        <a class="button-download" href="<?php echo $download["file_download"]; ?>" download="<?php echo esc_attr($download["label_download"]); ?>"><?php echo ($download["label_download"]); ?></a>
                                                    </div>
													<?php
												}
											}
											?>
                                        </div>
                                    </div>
									<?php
								}
							}
							?>
                        </div>
                    </div>
                </div><!-- /.light-wrap -->
			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div> <!-- page-content //-->

<?php get_footer();