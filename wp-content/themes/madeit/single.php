<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header(); ?>

    <div id="page-content" class="main no-hero" role="main">

<?php
global $post;
if ( have_posts() ) :
	/* Start the Loop */
	while ( have_posts() ) : the_post();
	setup_postdata($post);
	?>

        <div class="light-wrap-offset mt-small-fix ">
            <div class="container">
                <div class="row">

                    <div class="col-12 col-lg-8 mb-5 mb-lg-0 pb-xlarge">
                        <article class="news-type">
                            <?php if ( has_post_thumbnail( $post->ID ) ) { ?>
                            <div class="img-wrap main-img mb-3">
                            <?php echo get_the_post_thumbnail( $post->ID, 'content', array("class" => "img-fluid") ); ?>
                            </div>
                            <?php } ?>
                            <p class="date"><?php $post_date = get_the_date( 'j F Y' ); echo $post_date; ?></p>
                            <hgroup>
                                <h1 class="title">
                                    <?php the_title(); ?>
                                </h1>
                            </hgroup>
                            <div class="text">
                                <?php the_content(); ?>
                            </div>
                        </article>
                    </div>

                    <div class="col-12 col-lg-4 pb-xlarge aside-wrap responsive-bg-offset">
                        <aside>
                            <div class="row">
                                <div class="col-12 mb-4 mb-lg-2">
                                    <h4 class="aside-title">
                                        <?php _e("Altre notizie", "madeit"); ?>
                                    </h4>
                                </div>


                                    <?php
                                    $related = get_posts(
	                                    array(
		                                    'category__in' => wp_get_post_categories( $post->ID ),
		                                    'numberposts'  => 2,
		                                    'post__not_in' => array( $post->ID )
	                                    )
                                    );

                                    if( $related ) {
	                                    foreach( $related as $post ) {
		                                    setup_postdata($post);
		                                   ?>
                                            <div class="col-12 col-md-6 col-lg-12 mb-xxsmall">
                                                <?php get_template_part( 'template-parts/card/post' , get_post_format()); ?>
                                            </div>
                                            <?php
	                                    }
	                                    wp_reset_postdata();
                                    }
                                    ?>
                                <div class="col-12 text-right">
                                    <a href="<?php
                                    // not working on wpcom
                                    // $news = get_page_by_template( "page-templates/page-news.php" );
                                    //echo get_permalink( $news );
                                    echo home_url("news")
                                    ?>"  class="button-link"><?php _e("Vedi tutte le altre news", "madeit"); ?></a>
                                </div>
                            </div><!-- /.row-news -->

                        </aside>
                    </div><!-- /.col-spalla -->

                </div><!-- /.main-row -->
            </div><!-- /.main-container -->
        </div><!-- /.light-wrap -->

	<?php
	endwhile;
else :

	get_template_part( 'template-parts/content', '404' );

endif; ?>

    </div><!-- /.page-content -->



<?php get_footer();
