<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header();
?>
    <div id="page-content" class="main" role="main">

		<?php
		if((!is_front_page()) && (!is_archive()) ){
			get_template_part("template-parts/section/head-news");
		}
		?>

        <section>
            <div class="light-wrap-offset-down responsive-nobg">
                <div class="container">
                    <div class="row news-row card-wrap wrap-xxsmall">

						<?php
						if ( have_posts() ) {
							// Load posts loop.
							while ( have_posts() ) {
								the_post();
								echo '<div class="col-12 col-md-6 col-lg-4">';
								get_template_part( 'template-parts/card/post' , get_post_format());
								echo "</div>";
							}
						} ?>
                        <div class="col-12">
	                        <?php madeit_bootstrap_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
<?php get_footer();
