<section class="hero">
    <div class="container hero-caption">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                <div class="img-wrap mb-3">
                    <img src="<?php bloginfo("template_url"); ?>/assets/img/logo-h.svg" class="img-fluid" alt="MadeIT" />
                </div>
                <h2 class="title title-small mt-5">
                    <?php echo get_field("testo_hero_1") ?> <br class="d-none d-lg-block" />
	                <?php echo get_field("testo_hero_2") ?>
                </h2>
            </div>
            <?php if(get_field("label_button_1") || get_field("label_button_2")){ ?>
            <div class="col-12 mt-3 text-center d-none d-md-block">
                <?php if(get_field("label_button_1")){ ?>
                <a class="button m-2" href="<?php echo get_field("link_button_1" ); ?>"><?php echo get_field("label_button_1" ); ?></a>
                <?php
                 }
                 if(get_field("label_button_2")){ ?>
                <a class="button m-2" href="<?php echo get_field("link_button_2" ); ?>"><?php echo get_field("label_button_2" ); ?></a>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="container hero-footer">
        <div class="row">
            <?php if(!(get_field("label_button_1") || get_field("label_button_2"))){ ?>
            <div class="col-12 text-center">
                <a class="button-down" href="#scopri-madeit">
                    <span class="sr-only"><?php _e("Scopri cosa può fare MadeIT per la tua startup.","madeit"); ?></span>
                </a>
            </div>
            <?php } ?>
            <div class="col-12 text-right">
                <nav class="social-menu" aria-label="<?php echo get_field("testo_hero_1") ?>">
                    <span class="nav-intro">
            	      <?php echo get_field("testo_social", "options"); ?>
                    </span>
                    <ul class="nav nav-social nav-social--round justify-content-end">
						<?php  get_template_part("template-parts/common/social");  ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-fluid video-bg">
        <div class="row">
            <div class="col-12 px-0">
                <div class="video-wrap">
                    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                        <source src="<?php echo get_field("url_video") ?>" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </div>


    <div class="container hero-footer d-block d-md-none">
        <div class="row">
            <div class="col-12 text-center">
                <a class="button-down" href="#manifesto">
                    <span class="sr-only"><?php _e("Scopri cosa può fare MadeIT per la tua startup.", "madeit"); ?></span>
                </a>
            </div>
        </div>
    </div>


</section>
<?php
if(get_field("label_button_1") || get_field("label_button_2")){
?>
<section>
    <div class="col-12 text-center d-block d-md-none pt-4">
        <h4><?php echo get_field("cta_buttons"); ?></h4>
            <?php if(get_field("label_button_1")){ ?>
        <a class="button m-2" href="<?php echo get_field("link_button_1"); ?>"><?php echo get_field("label_button_1"); ?></a>
		    <?php }
		if(get_field("label_button_2")){ ?>
        <a class="button m-2" href="<?php echo get_field("link_button_2"); ?>"><?php echo get_field("label_button_2"); ?></a>
            <?php
        }
            ?>
    </div>
</section>
<?php
}