<section class="pt-lg-0 pb-lg-2 pb-xxlarge pt-xxlarge">
<div class="container">
		<div class="txtimg row">
            <div class="col-12 col-lg-8 mb-4 mb-5 mb-lg-0">
                <div class="img-wrap offset-container-left-lg">
					<img class="img-fluid" src="<?php echo get_field("immagine_ecosistema"); ?>" alt="<?php echo esc_attr(get_field("titolo_ecosistema")); ?>" />
				</div>
			</div>
			<div class="col-12 col-lg-4">
				<div class="hgroup">
					<strong class="supertitle"><?php echo get_field("titolo_ecosistema"); ?></strong>
					<h2 class="title"><?php echo get_field("sottotitolo_ecosistema"); ?></h2>
				</div>
				<div class="text">
					<?php echo get_field("testo_ecosistema"); ?>
				</div>
				<div class="reference">
					<a class="button full mr-5" href="<?php echo get_field("link_ecosistema"); ?>"><?php echo get_field("testo_link_ecosistema"); ?></a>
				</div>
			</div>
		</div>
	</div>
</section><?php
