<?php
global $partner;
?>
<section>
    <div class="light-wrap pt-xxlarge pb-5 pb-lg-0">

    <div class="container">
		<div class="txtimg row">
            <div class="col-12 col-lg-6 order-lg-1 mb-4 mb-5 mb-lg-0">
                <div class="img-wrap v-offset">
					<img class="img-fluid" src="<?php echo get_field("immagine_partner"); ?>" alt="<?php echo esc_attr(get_field("titolo_partner")); ?>" />
				</div>
			</div>
			<div class="col-12 col-lg-6 order-lg-0 mb-3">
				<div class="hgroup">
					<strong class="supertitle"><?php echo get_field("titolo_partner"); ?></strong>
					<h2 class="title"><?php echo get_field("sottotitolo_partner"); ?></h2>
				</div>
				<div class="text">
					<?php echo get_field("testo_partner"); ?>
				</div>
				<div class="reference">
					<a class="button  mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php
					$partner = get_page_by_template("page-templates/page-partner.php");
					echo get_permalink($partner);
					?>"><?php _e("Scopri di più","madeit"); ?></a>
<!--// <a target="_blank" class="button-link" href="<?php echo get_field("link_partner", "options"); ?>"><span><?php _e("Diventa partner","madeit"); ?></span></a> //-->
				</div>
			</div>
		</div>
	</div>
    </div>
    <?php
    $partners = get_field("program_partners", "options");

    if($partners){
	    shuffle($partners);
    ?>
	<div class="dark-wrap">
        <div class="container pr-0 pr-sm-3">
            <div class="hgroup pt-5">
                <h4 class="text-white" ><?php _e("Program Partner", "madeit"); ?></h4>
            </div>
            <div id="home-partner-sl" class="owl-carousel owl-theme pt-4">
                <?php

                foreach ($partners as $partner){
                    get_template_part("template-parts/card/partner");
                }
                ?>
			</div>
		</div>
	</div>
        <?php
    }
        ?>
</section><?php
