
<section class="pt-xlarge pb-xlarge clip">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="section-heading">
                    <div class="hgroup">
                        <strong class="supertitle"><?php _e("News", "madeit"); ?></strong>
                        <h2 class="title"><?php _e("Rimani aggiornato e non perderti le ultime news", "madeit"); ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-11 offset-lg-1">
                <div class="offset-container-right">
                    <div id="home-news-sl" class="owl-carousel owl-theme">
						<?php
						$args = array(
							'ignore_sticky_posts' => 1
						);

						query_posts($args);
						if ( have_posts() ) {

							// Load posts loop.
							while ( have_posts() ) {
								the_post();
								echo '<div class="item">';
								get_template_part( 'template-parts/card/post' , get_post_format());
								echo "</div>";
							}
						}
						?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <a href="<?php
                // not working on wp com
                    //      $news = get_page_by_template("page-templates/page-news.php");
                 //               echo get_permalink($news);
                echo home_url("news");
                ?>" class="button mx-auto mt-xxsmall"><?php _e("Leggi tutte le news", "madeit"); ?></a>
            </div>
        </div>
    </div>
</section>





