<section class="pt-large pb-large">
	<div class="container">
		<div class="row txtimg">
            <div class="col-12 col-lg-6 offset-lg-0 mb-4 mb-5 mb-lg-0">
				<div class="image-wrap">
					<img src="<?php echo get_field("immagine_manifesto"); ?>" class="img-fluid" alt="MadeIT manifesto" />
				</div>
			</div>
            <div class="col-12 col-lg-6">
                <div class="hgroup  mb-3 mb-md-0">
					<strong class="supertitle"><?php echo get_field("titolo_manifesto"); ?></strong>
					<h2 class="title"><?php echo get_field("sottotitolo_manifesto"); ?></h2>
				</div>
				<div class="text">
					<?php echo get_field("testo_manifesto"); ?>
				</div>
				<div class="reference">
					<a class="button mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php
    					$manifesto = get_page_by_template("page-templates/page-manifesto.php");
    					echo get_permalink($manifesto);
                    ?>"><?php _e("Scopri di più","madeit"); ?></a>
					<a target="_blank" class="button-link" href="<?php echo get_field("link_manifesto", "options"); ?>"><span><?php _e("Aderisci subito","madeit"); ?></span></a>
				</div>
			</div>
		</div>
	</div>
</section>
