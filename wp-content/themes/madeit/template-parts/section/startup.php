<section class="light-wrap pt-xxlarge pb-large">
    <a class="bm" id="scopri-madeit" name="scopri-madeit"></a>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading text-center">
                    <div class="hgroup">
                        <h2 class="title">
							<?php echo get_field("titolo_startup"); ?>
                        </h2>
                    </div>
                    <div class="text">
						<?php echo get_field("sottotitolo_startup"); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
			<?php
			$cards = get_field( "card_startup" );
			foreach ($cards as $card){
				?>
                <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                    <div class="card">
                        <div class="img-wrap">
                            <img src="<?php echo $card["immagine"]; ?>" class="img-fluid" alt="<?php echo esc_attr($card["titolo"]); ?>" />
                        </div>
                        <h3 class="title"><?php echo $card["titolo"]; ?></h3>
                        <div class="text">
                            <p><?php echo $card["testo"]; ?></p>
                        </div>
                        <a class="button <?php if(!$card["link"]) echo "disabled";  ?>"  href="<?php echo $card["link"]; ?>"><?php echo $card["testo_link"]; ?></a>
                    </div>
                </div>
				<?php
			}
			?>
        </div>
        <div class="row">
            <div class="col-12 text-right" style="position:relative;z-index:1;">
                <a class="button-big-link mt-medium" href="<?php
                $startup = get_page_by_template("page-templates/page-startup.php");
                echo get_permalink($startup);
                ?>">
                    <span class="text" ><?php _e("MadeIT per le <br /> startup", "madeit"); ?></span>
                    <span class="arrow"></span>
                </a>
            </div>
        </div>
    </div>
</section><?php
