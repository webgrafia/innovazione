<section class="page-hero no-mchange" style="background-image:url('<?php bloginfo("template_url"); ?>/media/img/hero-news.jpg');">
	<div class="container hero-caption">
		<div class="row">
			<div class="col-12 col-lg-10 col-xl-8">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active" aria-current="page"><?php _e("News", "madeit"); ?></li>
					</ol>
				</nav>
				<h2 class="title">
					<?php _e("Rimani aggiornato e non perderti le novità su MadeIT e sull'ecosistema dell'innovazione", "madeit"); ?>
				</h2>
				<div class="text">
					<p><?php _e("Foto, video ed approfondimenti di ciò che conta nel mondo tecnologico", "madeit"); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
