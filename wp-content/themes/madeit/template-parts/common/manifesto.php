<?php
/**
 * CTA al manifesto inclusa pre-footer in diverse pagine
 */
?>
<section>
	<div id="manifesto-ban" class="dark-wrap">
		<div class="caption container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="hgroup">
						<strong class="supertitle">
							<?php echo get_field("titolo_sezione_manifesto", "options"); ?>
						</strong>
						<h2 class="title"><?php echo get_field("sottotitolo_sezione_manifesto", "options"); ?></h2>
					</div>
					<a target="_blank" class="button large extradark" href="<?php echo get_field("link_manifesto", "options"); ?>"><?php echo get_field("testo_link_sezione_manifesto", "options"); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
