<?php
if($facebook = get_field("facebook", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $facebook; ?>" target="_blank">
			<span class="sr-only">Facebook</span>
			<i class="fab fa-facebook-f"></i>
		</a>
	</li>
	<?php
}
?>
<?php
if($instagram = get_field("instagram", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $instagram; ?>" target="_blank">
			<span class="sr-only">Instagram</span>
			<i class="fab fa-instagram"></i>
		</a>
	</li>
	<?php
}
?>
<?php
if($linkedin = get_field("linkedin", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $linkedin; ?>" target="_blank">
			<span class="sr-only">Linkedin</span>
			<i class="fab fa-linkedin-in"></i>
		</a>
	</li>
	<?php
}
?>
<?php
if($twitter = get_field("twitter", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $twitter; ?>" target="_blank">
			<span class="sr-only">Twitter</span>
			<i class="fab fa-twitter"></i>
		</a>
	</li>
	<?php
}
?>
<?php
if($youtube = get_field("youtube", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $youtube; ?>" target="_blank">
			<span class="sr-only">You Tube</span>
			<i class="fab fa-youtube"></i>
		</a>
	</li>
	<?php
}
?>
<?php
if($medium = get_field("medium", "options")){
	?>
	<li class="nav-item">
		<a class="nav-link" href="<?php echo $medium; ?>" target="_blank">
			<span class="sr-only">Medium</span>
			<i class="fab fa-medium"></i>
		</a>
	</li>
	<?php
}
?><?php
