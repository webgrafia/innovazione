<?php
global $post, $programmi;
?>
	<div class="row m-0 card-ita card-programma-<?php echo count($programmi); ?> <?php echo get_field("colore_programma", $post); ?> <?php if(!get_field("programma_attivo", $post)) echo "disabled"; ?>">
        <div class="col-12 col-md-6">
		<div class="img-wrap">
			<img src="<?php  the_field("logo_programma_card"); ?>" class="img-fluid" alt="<?php echo get_field("nome_programma", $post); ?>">
		</div>
		<div class="hgroup">
			<h2 class="title">
				<span class="big">MadeIT</span><?php echo get_field("nome_programma", $post); ?>
			</h2>
		</div>

        </div>
        <div class="col-12 col-md-6 vcenter">
            <div class="text text-left">
                <h3><?php echo get_the_excerpt(); ?></h3>
            </div>
            <div class="reference pt-3">
                <a class="button full simple <?php if(!get_field("programma_attivo", $post)) echo "disabled"; ?>" href="<?php echo get_permalink($post); ?>"><span><?php echo get_field("label_cta_programma", $post); ?></span></a>
            </div>
        </div>

	</div>
