<?php
global $partner;
$logo = get_field("logo", $partner);
if($logo){
//                        print_r($logo);
	?>
	<div class="card-logo-square">
		<a class="logo-wrap"  style="background-image:url('<?php echo $logo["url"]; ?>');">
			<span class="sr-only"><?php echo $partner->name; ?></span>
		</a>
	</div>
<?php }else{ ?>
	<div class="card-logo-square">
		<a class="logo-wrap text-center" >
			<h4 class="text-white mt-5 pt-4"><?php echo $partner->name; ?></h4>
		</a>
	</div>
	<?php
}