<?php
global $post;
?>
    <div class="card2">
        <div class="img-wrap">
	        <?php if ( has_post_thumbnail()){ ?>
            <a href="<?php the_permalink(); ?>">
		    <?php  the_post_thumbnail("card",  array('class' => 'img-fluid')); ?>
            </a>
            <?php
	        }else{
		        $embedded = get_media_embedded_in_content(apply_filters("the_content", $post->post_content), array(  'video', 'object', 'embed', 'iframe' ));
		        if($embedded){
			        ?>
			        <?php echo $embedded[0]; ?>
			        <?php
		        }
	        }
	        ?>
        </div>
        <div class="content-wrap">
            <p class="date"><?php $post_date = get_the_date( 'j F Y' ); echo $post_date; ?></p>
            <h3 class="title"><?php the_title(); ?></h3>
            <div class="text">
                <p><?php echo get_the_excerpt(); ?></p>
            </div>
            <div class="footer-wrap text-right">
                <a class="button-link" href="<?php the_permalink(); ?>"  ><span><?php _e("Leggi la news"); ?></span></a>
            </div>
        </div>
    </div>
