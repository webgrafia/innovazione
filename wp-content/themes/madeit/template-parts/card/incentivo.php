<?php
global $post;
$link = get_field("url_incentivo");
$link_normativa = get_field("url_normativa");

?>
    <div class="card2 incentivi w-100">
    <div class="content-wrap">
        <h3 class="title simple"><?php the_title(); ?></h3>
        <div class="tag-wrap">
		    <?php
		    // agevolazione
		    $agevolazioni = wp_get_object_terms($post->ID, "agevolazione");
		    //print_r($argomenti);
		    if($agevolazioni){
			    foreach ($agevolazioni as $agevolazione){
				    echo '<span class="tag">'.$agevolazione->name.'</span>';
			    }
		    }
		    $tipologie = wp_get_object_terms($post->ID, "tipologia");
		    if($tipologie){
			    foreach ($tipologie as $tipologia){
				    if (strpos($tipologia->slug, 'covid') !== false)
				        echo '<span class="tag label-covid">'.$tipologia->name.'</span>';
			    }
		    }
		    ?>
        </div>
        <div class="text">
			<?php the_excerpt(); ?>
        </div>
        <div class="footer-wrap row m-0">
            <div class="col-6 p-0">
                <?php  if($link_normativa){ ?>
                <a href="<?php echo $link_normativa; ?>" class="text-link" target="_blank"><?php _e("Normativa", "madeit"); ?></a>
                <?php } ?>
            </div>
            <div class="col-6 p-0 text-right" >
                <a href="<?php echo $link; ?>" class="button-link" target="_blank"><span><?php _e("Scopri di più", "madeit"); ?></span></a>
            </div>
        </div>
    </div>
    </div><?php
