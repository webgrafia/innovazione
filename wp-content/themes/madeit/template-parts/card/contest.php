<?php
global $post;

// partner
$partners = wp_get_object_terms($post->ID, "partner");
$logo = false;
if(is_array($partners) && count($partners) > 0){
	$partner = $partners[0];
	$image_p = get_field("logo", $partner);
	if($image_p)
		$logo = $image_p["url"];
}
?>
<div class="card2 contest">

    <div class="img-wrap empty">
		<?php  if($logo){ ?>
            <div class="logo" style="background-image:url('<?php echo $logo; ?>');"></div>
		<?php } ?>
    </div>
    <div class="content-wrap">
        <p class="date-range"><strong><?php _e("Inizio contest:", "madeit"); ?></strong> <?php
			echo date_i18n( 'D', get_field("data_inizio_contest") );
			echo " ";
			echo get_field("data_inizio_contest");

			?></p>
        <h3 class="title"><?php the_title(); ?></h3>
        <div class="tag-wrap">
			<?php
			// argomenti
			$argomenti = wp_get_object_terms($post->ID, "argomento");
			//print_r($argomenti);
			if($argomenti){
				foreach ($argomenti as $argomento){
					echo '<span class="tag">'.$argomento->name.'</span>';
				}
			}
			?>
        </div>
        <div class="text">
			<?php the_excerpt(); ?>
        </div>
        <div class="footer-wrap text-right">
            <a href="<?php the_permalink(); ?>" class="button-link"><span><?php _e("Scopri di più", "madeit"); ?></span></a>
        </div>
    </div>
</div>
