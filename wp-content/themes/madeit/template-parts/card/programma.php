<?php
global $post, $programmi;
?>
	<div class="card-ita card-programma-<?php echo count($programmi); ?> <?php echo get_field("colore_programma", $post); ?> <?php if(!get_field("programma_attivo", $post)) echo "disabled"; ?>">
		<div class="img-wrap">
			<img src="<?php  the_field("logo_programma_card"); ?>" class="img-fluid" alt="<?php echo get_field("nome_programma", $post); ?>">
		</div>
		<div class="hgroup">
			<h2 class="title">
				<span class="big">MadeIT</span><?php echo get_field("nome_programma", $post); ?>
			</h2>
		</div>
		<div class="text">
			<p><?php echo get_the_excerpt(); ?></p>
		</div>
		<div class="reference">
			<a class="button full simple <?php if(!get_field("programma_attivo", $post)) echo "disabled"; ?>" href="<?php echo get_permalink($post); ?>"><span><?php echo get_field("label_cta_programma", $post); ?></span></a>
		</div>
	</div>
