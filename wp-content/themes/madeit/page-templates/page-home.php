<?php
/* Template Name: Home */

get_header();
global $partner;
?>

	<div id="page-content" class="main" role="main">

	<?php  get_template_part("template-parts/section/hero");  ?>

	<?php  get_template_part("template-parts/section/manifesto");  ?>

	<?php  get_template_part("template-parts/section/startup");  ?>

	<?php  get_template_part("template-parts/section/ecosistema");  ?>

	<?php  get_template_part("template-parts/section/partner");  ?>

	<?php  get_template_part("template-parts/section/news");  ?>

	<?php  // get_template_part("template-parts/common/manifesto");  ?>

	</div> <!-- page-content //-->

<?php get_footer();


