<?php
/* Template Name: Full */

get_header(); ?>

    <div id="page-content" class="main no-hero" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>
                <div class="light-wrap pb-xxlarge">
                    <div class="container pt-large pb-5">
                        <div class="row">

                            <div class="col-12 col-xl-10 offset-xl-1">
                                <div class="section-heading top-page">
                                    <h2 class="title pr-0 pr-lg-5" >
										<?php the_title(); ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container">
                        <div class="row cards-v-wrap">

                            <div class="col-12 col-xl-10 offset-xl-1">
                                <div class="card-content-item px-0 pt-4">


									<?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.light-wrap -->
			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div> <!-- page-content //-->


<?php get_footer();