<?php
/* Template Name: Ecosistema */

get_header(); ?>
	<div id="page-content" class="main" role="main">

<?php
global $post;
if ( have_posts() ) :
	/* Start the Loop */
	while ( have_posts() ) : the_post();
		setup_postdata($post);
		?>


    <section class="page-hero" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
        <div class="container hero-caption">
            <div class="row">
                <div class="col-12 col-lg-10 col-xl-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                        </ol>
                    </nav>
                    <h2 class="title">
	                    <?php the_title(); ?>
                    </h2>
                    <div class="text">
                        <p><?php the_field("sommario"); ?></p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="d-none d-md-block">
        <div class="light-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="embed-responsive embed-responsive-21by9 map-embed">
                            <iframe  class="embed-responsive-item" src="<?php the_field("url_iframe"); ?>" title="Ecosistema">
                                <p>Your browser does not support iframes.</p>
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container pt-xxlarge pb-xlarge">
            <div class="row txtimg">
                <div class="col-12 col-lg-6 offset-lg-0 mb-4 mb-5 mb-lg-0">
                    <div class="image-wrap">
                        <img src="<?php the_field("immagine_cta"); ?>" class="img-fluid" alt="<?php the_field("titolo_cta"); ?>" />
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="hgroup  mb-3 mb-md-0">
                        <h2 class="title"><?php the_field("titolo_cta"); ?></h2>
                    </div>
                    <div class="text">
	                    <?php the_field("testo_cta"); ?>
                    </div>
                    <div class="reference">
                        <a target="_blank" class="button large px-5 mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php the_field("link_bottone_cta"); ?>"><?php the_field("label_bottone_cta"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="light-wrap pt-xxlarge pb-xxlarge d-md-none">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-xxsmall">
                        <h2 class="h3">
                            <?php _e("Scopri la mappa dell'ecosistema Italiano", "madeit"); ?>
                        </h2>
                    </div>
                    <div class="col-12 text-center mb-xsmall">
                        <img src="<?php bloginfo("template_url"); ?>/media/img/ecosistema-mappa-placeholder.png" class="img-fluid"  />
                    </div>

                    <div class="col-12">
                        <button class="button full" data-toggle="modal" data-target="#map-mobile"><?php _e("Visualizza la mappa", "madeit"); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="map-mobile" tabindex="-1" role="dialog" aria-labelledby="#modal-mobile-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <h5 class="modal-title sr-only" id="#modal-mobile-title"><?php _e("la mappa dell'ecosistema Italiano", "madeit"); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="fa fa-times"></i>
            </button>
            <div class="modal-body">
                <iframe src="<?php the_field("url_iframe"); ?>" title="Ecosistema">
                    <p>Your browser does not support iframes.</p>
                </iframe>
            </div>
        </div>
    </div>


	<?php
	endwhile;
else :

	get_template_part( 'template-parts/content', '404' );

endif; ?>

    </div>
<?php get_footer();


