<?php
/* Template Name: News */

get_header(); ?>
    <div id="page-content" class="main" role="main">

		<?php get_template_part("template-parts/section/head-news"); ?>

        <section>
            <div class="light-wrap-offset-down responsive-nobg">
                <div class="container">
                    <div class="row news-row card-wrap wrap-xxsmall">

						<?php
						$args = array(
							'ignore_sticky_posts' => 1
						);
						query_posts($args);
						if ( have_posts() ) {

							// Load posts loop.
							while ( have_posts() ) {
								the_post();
								echo '<div class="col-12 col-md-6 col-lg-4">';
								get_template_part( 'template-parts/card/post' , get_post_format());
								echo "</div>";
							}
						} ?>
                        <div class="col-12">
		                    <?php madeit_bootstrap_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>


<?php get_footer();
