<?php
/* Template Name: Thanks Form Contest */

get_header(); ?>

    <div id="page-content" class="main no-hero" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);


				// normalizzo tutte le post arrivate
				foreach ( $_REQUEST as $key => $value ) {
				    $_REQUEST[$key] = urldecode($value);
				}
				?>
                <div class="light-wrap pb-xxlarge">
                    <div class="container pt-large pb-5 no-print">
                        <div class="row">

                            <div class="col-12 col-md-6 text-left text-md-right mb-2 px-5">
                                <strong class="supertitle">Congratulazioni!</strong>
                                <h3 class="text-sans-serif text-uppercase" style="font-weight: 900;">Il tuo messaggio &egrave; <br>stato spedito</h3>

                            </div>
                            <div class="col-12 col-md-6  px-5">
                                <div class="card" style="max-width: 350px;">
                                    <div class="row">
                                        <div class="col-3">
                                            <a class="download-ico" href="#">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-pdf.svg" alt="pdf" class="img-fluid" />
                                            </a>
                                        </div>
                                        <div class="col-9">
                                            <h5 class="text-uppercase font-weight-bold m-0">Stampa il riepilogo del form</h5>
                                            <div class="footer-wrap text-right">
                                                <a class="button-link button-download mt-1" href="javascript: window.print();"><span>Stampa subito</span></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="container">
                        <div class="row cards-v-wrap">

                            <div class="col-12 col-xl-10 offset-xl-1">
                                <div class="card-content-item px-0 pt-4">
									<?php
                                        the_content();
                                    ?>
                                    <div class="bg-madeit">
                                        <div class="col-10 offset-1 form-title">
                                            Profilo del referente
                                        </div>
                                    </div>

                                    <div class="col-10 offset-1 madeitform">
                                        <div class="row py-5">

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Nome</b><br>
                                                <?php echo $_REQUEST["nome"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Cognome</b><br>
                                                <?php echo $_REQUEST["nome"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Email</b><br>
                                                <?php echo $_REQUEST["email"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Telefono</b><br>
                                                <?php echo $_REQUEST["telefono"]; ?>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="bg-madeit">
                                        <div class="col-10 offset-1 form-title">
                                            Profilo del proponente
                                        </div>
                                    </div>

                                    <div class="col-10 offset-1 madeitform">
                                        <div class="row py-5">

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Tipologia</b><br>
                                                <?php echo $_REQUEST["Tipologia"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Denominazione</b><br>
                                                <?php echo $_REQUEST["denominazione"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Codice fiscale</b><br>
                                                <?php echo $_REQUEST["codice-fiscale"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Partita IVA</b><br>
                                                <?php echo $_REQUEST["piva"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4 unlock-next">
                                                <b>Il proponente ha sede o operativa legale in Italia</b><br>
	                                            <?php echo $_REQUEST["sede-in-italia"]; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4 disabled">
                                                <b>Indirizzo sede legale o operativa</b><br>
	                                            <?php if($_REQUEST["indirizzo"]) echo $_REQUEST["indirizzo"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 py-4 border-bottom">
                                                <b>Il proponente è l'organizzatore principale del Tech Contest</b><br>
	                                            <?php if($_REQUEST["organizzatore"]) echo $_REQUEST["organizzatore"]; else echo "- "; ?>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="bg-madeit">
                                        <div class="col-10 offset-1 form-title">
                                            Profilo del tech contest
                                        </div>
                                    </div>

                                    <div class="col-10 offset-1">
                                        <div class="row py-5">

                                            <div class="col-12 mb-4">
                                                <p class="form-subtitle"> Definiamo insieme, con alcune caratteristiche fondamentali, il tech contest. Questo favorirà un miglior matching con i beneficiari dell’iniziativa</p>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom unlock-next">
                                                <b>Il tech contest prevede una competiziona finalizzata ad almeno uno dei tre obiettivi: selezione di progetti, prodotti o servizi esistenti; sviluppo di nuove idee, progetti, prodotti o servizi; costituzione di nuove imprese ad alto potenziale tecnologico e innovativo</b><br>
	                                            <?php if($_REQUEST["competizione"]) echo $_REQUEST["competizione"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom disabled">
                                                <b>Indica a quale/i dei tre obiettivi è finalizzato il tech contest</b><br>
	                                            <?php if($_REQUEST["obiettivi"]) echo $_REQUEST["obiettivi"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom unlock-next">
                                                <b>Il tech contest ha ad oggetto tematiche connesse all’innovazione digitale o allo sviluppo di nuove tecnologie</b><br>
	                                            <?php if($_REQUEST["innovazione"]) echo $_REQUEST["innovazione"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom disabled">
                                                <b>Il tech contest prevede un ambito di applicazione della tecnologia che rispetta i principi dell’etica e della sostenibilità</b><br>
	                                            <?php if($_REQUEST["etica"]) echo $_REQUEST["etica"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom unlock-next">
                                                <b>Il tech contest ha come beneficiari aspiranti imprenditori, startup innovative o PMI innovative</b><br>
	                                            <?php if($_REQUEST["per-imprese"]) echo $_REQUEST["per-imprese"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom disabled">
                                                <b>Indica a quale dei tre beneficiari è indirizzato il tech contest</b><br>
	                                            <?php if($_REQUEST["beneficiari"]) echo $_REQUEST["beneficiari"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 py-4 border-bottom">
                                                <b>Il tech contest prevede una partecipazione gratuita da parte dei beneficiari</b><br>
	                                            <?php if($_REQUEST["free"]) echo $_REQUEST["free"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom unlock-next">
                                                <b>Il tech contest prevede un premio per i vincitori della competizione</b><br>
	                                            <?php if($_REQUEST["premio"]) echo $_REQUEST["premio"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 py-4 border-bottom disabled">
                                                <b>Indica la tipologia di premio</b><br>
	                                            <?php if($_REQUEST["tipologia-premio"]) echo $_REQUEST["tipologia-premio"]; else echo "- "; ?>

                                                <br><br>
                                                <b>Descrivi i servizi offerti</b>
                                                <br>
	                                            <?php if($_REQUEST["servizi-offerti"]) echo $_REQUEST["servizi-offerti"]; else echo "- "; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bg-madeit">
                                        <div class="col-10 offset-1 form-title">
                                            Profilo del tech contest - Info pubbliche
                                        </div>
                                    </div>

                                    <div class="col-10 offset-1 madeitform">
                                        <div class="row py-5">

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Nome del tech contest</b><br>
	                                            <?php if($_REQUEST["nome-tech-contest"]) echo $_REQUEST["nome-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Breve descrizione del tech contest</b><br>
	                                            <?php if($_REQUEST["slogan-tech-contest"]) echo $_REQUEST["slogan-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 mb-4">
                                                <b>Obiettivo del tech contest</b><br>
	                                            <?php if($_REQUEST["obiettivo-tech-contest"]) echo $_REQUEST["obiettivo-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 mb-4">
                                                <b>Funzionamento/regolamento del tech contest</b><br>
	                                            <?php if($_REQUEST["regolamento-tech-contest"]) echo $_REQUEST["regolamento-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 mb-4">
                                                <b>Beneficiari del tech contest</b><br>
	                                            <?php if($_REQUEST["beneficiari-tech-contest"]) echo $_REQUEST["beneficiari-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Data inizio iscrizioni </b>
                                                <br>
	                                            <?php if($_REQUEST["data-inizio-iscrizioni"]) echo $_REQUEST["data-inizio-iscrizioni"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Data fine iscrizioni </b>
                                                <br>
	                                            <?php if($_REQUEST["data-fine-iscrizioni"]) echo $_REQUEST["data-fine-iscrizioni"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Data inizio tech contest </b>
                                                <br>
	                                            <?php if($_REQUEST["data-inizio-tech-contest"]) echo $_REQUEST["data-inizio-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Data fine tech contesti </b>
                                                <br>
	                                            <?php if($_REQUEST["data-fine-tech-contest"]) echo $_REQUEST["data-fine-tech-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                    <b>Argomenti / Ambiti</b><br>
	                                                <?php if($_REQUEST["argomenti"]) echo $_REQUEST["argomenti"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 col-md-6 mb-4">
                                                <b>Link del tech contest</b><br>
	                                            <?php if($_REQUEST["link-contest"]) echo $_REQUEST["link-contest"]; else echo "- "; ?>
                                            </div>

                                            <div class="col-12 upload-dragdrop">
                                                <div class="upload-dragdrop-image">
                                                    <img src="https://italia.github.io/bootstrap-italia/dist/assets/upload-drag-drop-icon.svg" alt="imagealt" aria-hidden="true">
                                                </div>
                                                <div class="upload-dragdrop-text">
                                                    <h5>Logo del tech contest o del proponente</h5>
                                                    <?php
                                                    if($_REQUEST["logo-contest"])
                                                        echo "Logo caricato correttamente";
                                                    else
	                                                    echo "Logo non allegato";
                                                    ?>
                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                    <div class="bg-madeit">
                                        <div class="col-10 offset-1 form-title">
                                            Dichiarazioni
                                        </div>
                                    </div>

                                    <div class="col-10 offset-1 madeitform bolder">
                                        <div class="row py-5">
                                            <div class="col-12 mb-4">
                                                <p class="form-subtitle">Consapevole delle responsabilità, anche penali, previste per le ipotesi di falsità in atti e dichiarazioni mendaci ai sensi degli articoli 75 e 76 del D.P.R. n. 445/2000 si dichiara:</p>
                                            </div>
                                            <div class="col-12 mb-4">
                                                <ul>
                                                    <li>di essere consapevole che quanto dichiarato potrà essere oggetto di verifica da parte dell’Amministrazione</li>
                                                    <li>che il soggetto proponente è in possesso dei requisiti previsti dall'Avviso pubblico TechContestPartnerMadeIT</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.light-wrap -->
			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div> <!-- page-content //-->


<?php get_footer();