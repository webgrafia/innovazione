<?php
/* Template Name: Startup */

get_header(); ?>
    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>

                <section class="page-hero submenu-control" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-12 col-lg-8 col-xl-6 pr-xl-5">
                                <nav aria-label="breadcrumb" >
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <div class="text">
                                    <p><?php the_field("sommario"); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <nav id="page-submenu" class="navbar navbar-expand d-none d-md-block" aria-label="<?php _e("Programmi", "madeit"); ?>">
                    <div class="container">
                        <ul class="page-submenu bm-menu navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="#programmi-eccellenza"><?php _e("Programmi di eccellenza", "madeit"); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#incentivi"><?php _e("Incentivi", "madeit"); ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tech-contest"><?php _e("Tech contest", "madeit"); ?></a>
                            </li>
                        </ul>
                    </div>
                </nav>


                <section class="bm-section pt-xxlarge">
                    <a class="bm" name="programmi-eccellenza" id="programmi-eccellenza"></a>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                                <div class="section-heading text-center">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php the_field("titolo_programma"); ?>
                                        </h2>
                                    </div>
                                    <div class="text">
                                        <p>
											<?php the_field("sottotitolo_programma"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

	                    <?php
	                    global $post;
	                    $origpost = $post;
	                    $programmi = get_field("programmi"); ?>
                        <div class="row justify-content-center v-offset-card">
		                    <?php
		                    foreach ( $programmi as $item ) {
		                        $post = $item["programma"];
			                    setup_postdata($post);
			                    ?>
                                <div class="col-12 <?php if(count($programmi) == 3) echo "col-md-6 col-lg-4"; else if(count($programmi) == 2) echo "col-md-6 col-lg-6"; else echo "col-md-9"; ?>  mb-5 mb-lg-0">
				                    <?php get_template_part("template-parts/card/programma", count($programmi)); ?>
                                </div>

			                    <?php
		                    }
		                    $post = $origpost;
		                    wp_reset_postdata();
		                    ?>
                        </div>
                    </div>
                </section>




                <section class="bm-section light-wrap pt-doublelarge pb-xlarge">
                    <a class="bm" name="incentivi" id="incentivi"></a>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                                <div class="section-heading text-center">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php the_field("titolo_incentivo"); ?>
                                        </h2>
                                    </div>
                                    <div class="text">
                                        <p>
											<?php the_field("sottotitolo_incentivo"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row card-wrap">

							<?php
							$query_incentivo = new WP_Query();
							$args = array(
								'post_type' => "incentivo",
								"posts_per_page" => 3
							);

							$query_incentivo->query($args);
							if ( $query_incentivo->have_posts() ) {
								// Load posts loop.
								while ( $query_incentivo->have_posts() ) {
									$query_incentivo->the_post();
									echo '<div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">';
									get_template_part( 'template-parts/card/incentivo' );
									echo "</div>";
								}
							}
							wp_reset_query();
							wp_reset_postdata();
							?>
                        </div>
                        <div class="row">


	                            <?php
                                if(wp_count_posts("incentivo")->publish > 0)
                                {
                                ?>
                            <div class="col-12 text-center mt-lg-5">
                                <a class="button large" href="<?php echo get_post_type_archive_link("incentivo"); ?>">
									<?php _e("Scopri tutti gli incentivi", "madeit"); ?>
                                </a>
                            </div>
	                            <?php
	                            }else{
		                            ?>
                            <div class="col-12 text-center mt-0">
                                    <img class="clessidra mb-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/clessidra.svg">
                                    <a class="button large disabled" href="#0">
			                            <?php _e("coming soon", "madeit"); ?>
                                    </a>
                            </div>
		                            <?php
	                            }
	                            ?>

                        </div>
                    </div>
                </section>



                <section class="bm-section pt-doublelarge pb-xlarge">
                    <a class="bm" name="texh-contest" id="tech-contest"></a>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                                <div class="section-heading text-center">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php the_field("titolo_contest"); ?>
                                        </h2>
                                    </div>
                                    <div class="text">
                                        <p>
											<?php the_field("sottotitolo_contest"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row card-wrap">
							<?php
                            global $post;
                            $origpost = $post;
                            $contests = madeit_list_contests(3);
							foreach ( $contests as $post ) {
                                setup_postdata($post);
								echo '<div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">';
								get_template_part( 'template-parts/card/contest' );
								echo "</div>";
							}
							$post = $origpost;
							wp_reset_postdata();
							?>
                        </div>
                        <div class="row">

                                <?php
                                if(wp_count_posts("contest")->publish > 0){
                                ?>
                            <div class="col-12 text-center mt-lg-5">
                                <a class="button large" href="<?php echo get_post_type_archive_link("contest"); ?>">
									<?php _e("Scopri tutti i contest", "madeit"); ?>
                                </a>
                            </div>
                                    <?php
                                }else{
                                    ?>
                            <div class="col-12 text-center mt-0">
                                    <img class="clessidra mb-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/clessidra.svg">
                                    <a class="button large disabled" href="#0">
		                                <?php _e("coming soon", "madeit"); ?>
                                    </a>
                            </div>
                                        <?php
                                }
                                    ?>


                        </div>

                    </div>
                </section>



				<?php  // get_template_part("template-parts/common/manifesto");  ?>


			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>
<?php get_footer();


