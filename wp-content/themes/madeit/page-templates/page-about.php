<?php
/* Template Name: Chi Siamo */

get_header(); ?>


    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>

                <section class="page-hero" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-12 col-lg-10 col-xl-8">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <div class="text">
                                    <p><?php the_field("sommario"); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="pt-doublelarge pb-doublelarge">
                    <div class="container">
                        <div class="row txtimg">
                            <div class="col-lg-7 order-lg-1 mb-5 mb-lg-0">
                                <?php if(get_field("video_intro")){  ?>
                                <div class="video-wrap">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe title="Intro" class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/<?php the_field("video_intro"); ?>" allowfullscreen></iframe>
                                    </div>
                                    <!-- fine video embed -->
                                </div>
                                    <?php
                                }else{
                                    ?>
                                    <div class="img-wrap">
                                        <img class="img-fluid" src="<?php the_field("immagine_intro"); ?>" alt="Intro" />
                                    </div>
                                    <?php
                                }
                                ?>
                                <p class="dida pt-1 text-center"><?php the_field("dida_video_intro"); ?></p>
                            </div>
                            <div class="col-lg-5 order-lg-0">
                                <div class="hgroup">
                                    <h2 class="title"><?php the_field("titolo_intro"); ?></h2>
                                </div>
                                <div class="text">
                                    <p><?php the_field("testo_intro"); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="light-wrap pt-xxlarge pb-xxlarge">
                        <div class="container">

                            <div class="row">
                                <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                                    <div class="section-heading text-center">
                                        <div class="hgroup">
                                            <h2 class="title">
												<?php the_field("titolo_ministri"); ?>
                                            </h2>
                                        </div>
                                        <div class="text">
                                            <p><?php the_field("testo_ministri"); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row align-items-stretch justify-content-center wrap-xxsmall pb-medium">
								<?php
								$loghi_ministri = get_field("loghi_ministri");
								foreach ( $loghi_ministri as $logo ) { ?>
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="card-logo">
                                            <div class="img-wrap">
                                                <a href="<?php echo  $logo["link"]; ?>" target="_blank"><img class="img-fluid" src="<?php echo  $logo["logo"]; ?>" alt="Logo" /></a>
                                            </div>
                                        </div>
                                    </div>

								<?php } ?>
                            </div>
                        </div>
                    </div>
                </section>



                <section class="pt-xxlarge pb-xxlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="section-heading text-center">
                                    <div class="hgroup">
                                        <h2 class="title"><?php the_field("titolo_madeit"); ?></h2>
                                    </div>
                                    <div class="text">
										<?php the_field("testo_madeit"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row  wrap-xxsmall">

							<?php
							$loghi_madeit = get_field("loghi_madeit");
							foreach ( $loghi_madeit as $logo ) { ?>
                                <div class="col-6 col-md-4 col-lg-4">
                                    <div class="card-logo">
                                        <div class="img-wrap">
                                            <a href="<?php echo  $logo["link"]; ?>" target="_blank"><img class="img-fluid" src="<?php echo  $logo["logo"]; ?>" alt="Logo" /></a>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="light-wrap pt-xxlarge pb-xxlarge">
                        <div class="container">
                            <div class="row txtimg pb-medium">
                                <div class="col-12 col-lg-6 mb-4 mb-5 mb-lg-0">
                                    <div class="img-wrap offset-container-left-lg">
                                        <img class="img-fluid" src="<?php the_field("immagine_ministero"); ?>"  alt="Ministero"/>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="hgroup">
                                        <strong class="supertitle"><?php the_field("occhiello_ministero"); ?></strong>
                                        <h2 class="title" id="titolo-ministero"><?php the_field("titolo_ministero"); ?></h2>
                                    </div>
                                    <div class="text">
										<?php the_field("testo_ministero"); ?>
                                    </div>
                                    <div class="reference">
                                        <nav class="social-menu" aria-labelledby="titolo-ministero">
                                            <ul class="nav nav-social nav-social--square invert">
												<?php
												$social_ministero = get_field("social_ministero");
												foreach ($social_ministero as $social){
													?>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="<?php echo $social["url"]; ?>" target="_blank">
                                                            <span class="sr-only"><?php echo $social["social"]; ?></span>
                                                            <i class="fab <?php echo $social["icona"]; ?>"></i>
                                                        </a>
                                                    </li>
													<?php
												}
												?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                            <?php
                            /**

                            <div class="row txtimg align-items-start">
                                <div class="col-12 col-lg-4 offset-lg-1 mb-4 mb-5 mb-lg-0">
                                    <div class="img-wrap">
                                        <img class="img-fluid" src="<?php the_field("immagine_ministro"); ?>" />
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="hgroup">
                                        <strong class="supertitle"><?php the_field("occhiello_ministro"); ?></strong>
                                        <h2 class="title"><?php the_field("titolo_ministro"); ?></h2>
                                    </div>
                                    <div class="text">
										<?php the_field("testo_ministro"); ?>
                                    </div>
                                    <div class="reference">
                                        <nav class="social-menu">
                                            <ul class="nav nav-social nav-social--square invert">
												<?php
												$social_ministro = get_field("social_ministro");
												foreach ($social_ministro as $social){
													?>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="<?php echo $social["url"]; ?>" target="_blank">
                                                            <span class="sr-only"><?php echo $social["social"]; ?></span>
                                                            <i class="fab <?php echo $social["icona"]; ?>"></i>
                                                        </a>
                                                    </li>
													<?php
												}
												?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>

                             **/
                              ?>
                        </div>
                    </div>
                </section>

                <section class="pt-xxlarge">
                    <div class="container">

                        <div class="row">
                            <div class="col-12 col-lg-8 offset-lg-2">
                                <div class="section-heading text-center large-space">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php the_field("titolo_startup"); ?>
                                        </h2>
                                    </div>
                                    <div class="text">
										<?php the_field("testo_startup"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="txtimg row  pb-xsmall">
                            <div class="col-12 col-lg-6 mb-4 mb-5 mb-lg-0">
                                <div class="img-wrap">
                                    <h3 class="block-title"><?php the_field("label_immagine_startup"); ?></h3>
                                    <img class="img-fluid" src="<?php the_field("immagine_startup"); ?>" alt="Startup" />
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <div class="hgroup">
                                    <h4 class="subtitle"><?php the_field("sottotitolo_1_startup"); ?></h4>
                                </div>
                                <div class="text">
                                    <p><?php the_field("paragrafo_1_startup"); ?></p>
                                </div>
                                <div class="hgroup">
                                    <h4 class="subtitle"><?php the_field("sottotitolo_2_startup"); ?></h4>
                                </div>
                                <div class="text">
                                    <p><?php the_field("paragrafo_2_startup"); ?></p>
                                </div>
                                <div class="reference">
                                    <a class="button large mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php the_field("link_bottone_startup"); ?>">
										<?php the_field("testo_bottone_startup"); ?></a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="light-wrap pt-xsmall pb-xsmall">
                        <div class="container">
                            <div class="txtimg row">
                                <div class="col-12 col-lg-6 order-lg-1 mb-4 mb-5 mb-lg-0">
                                    <div class="img-wrap">
                                        <h3 class="block-title"><?php the_field("label_immagine_partner"); ?></h3>
                                        <img class="img-fluid" src="<?php the_field("immagine_partner"); ?>"  alt="Partner"/>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6 order-lg-0">
                                    <div class="hgroup">
                                        <h4 class="subtitle"><?php the_field("sottotitolo_1_partner"); ?></h4>
                                    </div>
                                    <div class="text">
                                        <p><?php the_field("paragrafo_1_partner"); ?></p>
                                    </div>
                                    <div class="hgroup">
                                        <h4 class="subtitle"><?php the_field("sottotitolo_2_partner"); ?></h4>
                                    </div>
                                    <div class="text">
                                        <p><?php the_field("paragrafo_2_partner"); ?></p>
                                    </div>
                                    <div class="reference">
                                        <a class="button large  mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php the_field("link_bottone_partner"); ?>"><?php the_field("testo_bottone_partner"); ?></a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>


			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>



<?php get_footer();


