<?php
/* Template Name: Contatti */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>

                <section class="page-hero no-mchange"  style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-12 col-lg-10 col-xl-8">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="h1-fix">
									<?php the_title(); ?>
                                </h2>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="sidebar-wrap sidebar-fix">
                        <div class="container-fluid container-sidebar d-none d-md-block">
                            <div class="row justify-content-end">
                                <div class="col col-sidebar">
                                    <div class="pin-content pt-large pb-small">
                                        <div class="sidebar-index-wrap single-counter">
                                            <strong class="title"><?php _e("Contattaci", "madeit"); ?></strong>
                                            <ol class="index">
                                                <li><a href="#dove-scriverci"><?php the_field("titolo_dove_scriverci"); ?></a></li>
                                                <li><a href="#canali-social"><?php the_field("titolo_canali_social"); ?></a></li>
	                                            <?php
	                                            if(get_field("abilita_presskit")){
		                                            ?>
                                                    <li><a href="#presskit"><?php the_field("titolo_presskit"); ?></a></li>
		                                            <?php
	                                            }
	                                            ?>
                                                <?php
												if(get_field("abilita_feedback")){
													?>
                                                    <li><a href="#feedback"><?php the_field("titolo_feedback"); ?></a></li>
													<?php
												}
												?>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sidebar-main light-wrap">
                            <div class="container contact-card v-offset pb-xlarge">
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <div class="card  pb-2">
                                            <div class="d-flex flex-wrap align-items-center">
                                                <div class="img-wrap col-4 offset-4 col-lg-2 offset-lg-0 px-0">
                                                    <img src="<?php bloginfo("template_url"); ?>/assets/img/ico-pin.svg" class="img-fluid" alt="Mappa" />
                                                </div>
                                                <div class="col-12 col-md-12 col-lg-9 col-xl-8 offset-lg-1">
                                                    <h2 class="h1 text-left"><?php _e("Dove trovarci", "madeit"); ?></h2>
                                                    <div class="text text-left">
                                                        <p><?php the_field("sommario"); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="scroll-item-wrap pt-xxsmall">
                                <a class="bm" id="dove-scriverci" name="dove-scriverci"></a>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-7 offset-1">
                                            <div class="section-heading">
                                                <div class="hgroup">
                                                    <h2 class="title">
														<?php the_field("titolo_dove_scriverci"); ?>
                                                    </h2>
                                                </div>
                                                <div class="text  fs-big">
                                                    <p><?php the_field("testo_dove_scriverci"); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="scroll-item-wrap white-bg pt-xlarge pb-xxlarge">
                                <a class="bm" id="canali-social" name="canali-social"></a>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-9">
                                            <div class="section-heading text-center">
                                                <div class="hgroup">
                                                    <h2 class="title">
														<?php the_field("titolo_canali_social"); ?>
                                                    </h2>
                                                </div>
                                                <div class="text">
                                                    <p><?php the_field("testo_canali_social"); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $socials = get_field("socials");
                                        $c=0;
                                        foreach ( $socials as $social ) {
                                            $c++;
                                            $blocco = $social;

                                            ?>
                                            <div class="col-12 col-md-8 col-lg-4 mb-5">
                                                <div class="card px-4">
                                                    <div class="img-wrap">
                                                        <img src="<?php echo $blocco["logo"]; ?>" alt="<?php echo $item["nome"]; ?>" class="img-fluid max-height-100" />
                                                    </div>
                                                    <nav class="social-menu" aria-label="<?php echo $blocco["label"]; ?>">
                                                        <span class="nav-intro px-0"><?php echo $blocco["label"]; ?></span>
                                                        <ul class="nav nav-social nav-social--square nav-social--square--small invert">
					                                        <?php
					                                        foreach ( $blocco["social"] as $item ) {
						                                        ?>
                                                                <li class="nav-item">
                                                                    <a class="nav-link" href="<?php echo $item["url"]; ?>" target="_blank">
                                                                        <span class="sr-only"><?php echo $item["nome"]; ?></span>
                                                                        <i class="fab <?php echo $item["icona"]; ?>"></i>
                                                                    </a>
                                                                </li>

						                                        <?php
					                                        }
					                                        ?>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
	                                        <?php if(($c == 2) || ($c == 4) || ($c == 6)){ ?>
                                            </div>
                                            <div class="row">
                                            <?php
                                            }

                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

	                        <?php if(get_field("abilita_presskit")){ ?>
                                <div class="scroll-item-wrap  pt-doublelarge pb-xxlarge">
                                    <a class="bm" id="presskit" name="presskit"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-7 offset-lg-1">
                                                <div class="card pb-2">
                                                    <div class="d-flex flex-wrap align-items-center">
                                                        <div class="img-wrap col-6 offset-3 col-lg-3 offset-lg-0 px-0">
                                                            <img src="<?php bloginfo("template_url"); ?>/assets/img/presskit.svg" class="img-fluid" alt="Press Kit" />
                                                        </div>
                                                        <div class="col-12 col-lg-7 offset-lg-2">
                                                            <h2 class="h1 text-left"><?php the_field("titolo_presskit"); ?></h2>
                                                            <div class="text text-left">
                                                                <p><?php the_field("testo_presskit"); ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 reference text-right mb-3 pr-0">
                                                            <a class="button-link" href="<?php the_field("url_presskit"); ?>"><span><?php _e("Scopri di più", "madeit"); ?></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
	                        <?php } ?>

							<?php if(get_field("abilita_feedback")){ ?>
                                <div class="scroll-item-wrap  pt-doublelarge pb-xxlarge">
                                    <a class="bm" id="feedback" name="feedback"></a>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-7 offset-lg-1">
                                                <div class="card pb-2">
                                                    <div class="d-flex flex-wrap align-items-center">
                                                        <div class="img-wrap col-6 offset-3 col-lg-2 offset-lg-0 px-0">
                                                            <img src="<?php bloginfo("template_url"); ?>/assets/img/ico-feedback.svg" class="img-fluid" alt="Forum" />
                                                        </div>
                                                        <div class="col-12 col-lg-7 offset-lg-1">
                                                            <h2 class="h1 text-left"><?php the_field("titolo_feedback"); ?></h2>
                                                            <div class="text text-left">
                                                                <p><?php the_field("testo_feedback"); ?></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 reference text-right mb-3 pr-0">
                                                            <a class="button-link" href="<?php the_field("url_feedback"); ?>"><?php _e("Vai al forum", "madeit"); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
							<?php } ?>




                        </div>
                </section>

			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>



<?php get_footer();


