<?php
/* Template Name: Partner */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		global $partner;

		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>

                <section class="page-hero" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-12 col-lg-10 col-xl-8">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <div class="text">
                                    <p><?php the_field("sommario"); ?></p>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="container hero-footer d-none d-md-block">
                        <div class="row">
                            <div class="col-12 text-center">
                                <a class="button-down" href="#manifesto">
                                    <span class="sr-only"><?php _e("Scopri cosa può fare MadeIT per la tua startup.", "madeit"); ?></span>
                                </a>
                            </div>
                        </div>
                    </div>

                </section>


                <section class="white-bg pt-xxlarge pb-large">
                    <a class="bm" id="scopri-madeit" name="scopri-madeit"></a>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-3">
                                <div class="section-heading text-center">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php echo get_field("titolo_tipologia"); ?>
                                        </h2>
                                    </div>
                                    <div class="text">
										<?php echo get_field("sottotitolo_tipologia"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
							<?php
							$cards = get_field( "card_tipologia" );
							foreach ($cards as $card){
								?>
                                <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                                    <div class="card">
                                        <div class="img-wrap">
                                            <img src="<?php echo $card["immagine"]; ?>" class="img-fluid" alt="<?php echo esc_attr($card["titolo"]); ?>" />
                                        </div>
                                        <h3 class="title"><?php echo $card["titolo"]; ?></h3>
                                        <div class="text multiplep">
											<?php echo $card["testo"]; ?>
                                        </div>
                                        <a class="button <?php if(!$card["link"]) echo "disabled";  ?>"  href="<?php echo $card["link"]; ?>"><?php echo $card["testo_link"]; ?></a>
                                    </div>
                                </div>
								<?php
							}
							?>
                        </div>
                    </div>
                </section>


                <section class="light-wrap pt-large" id="sezione-program-partner">
                    <div class="container">

                        <div class="row txtimg">
                            <div class="col-12 col-lg-6 offset-lg-0 mb-4 mb-5 mb-lg-0">
                                <div class="image-wrap  v-offset">
                                    <img src="<?php echo get_field("foto_program_partner"); ?>" class="img-fluid" alt="MadeIT" />
                                </div>
                            </div>
                            <div class="col-12 col-lg-5">
                                <div class="hgroup  mb-4 mb-md-0">
                                    <h2 class="title"><?php the_field("titolo_program_partner"); ?></h2>
                                </div>
                                <div class="text">
                                    <p><?php the_field("testo_program_partner"); ?></p>
                                </div>
                                <div class="undertext mb-2">
									<?php the_field("avvertenze_program_partner"); ?>
                                </div>
                                <div class="reference mb-5">
									<?php
									if(get_field("link_avviso_program_partner")){
										?>
                                        <a target="_blank" class="button-link mr-5 mt-4" href="<?php echo get_field("link_avviso_program_partner"); ?>"><span><?php _e("Visualizza avviso pubblico","madeit"); ?></span></a>
										<?php
									}
									if(get_field("link_allegato_program_partner")){
										?>
                                        <a class="button-link mt-4" href="<?php echo get_field("link_allegato_program_partner"); ?>"><span><?php _e("Invia la tua proposta","madeit"); ?></span></a>
										<?php
									}
									?>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
					unset($partners);
					$partners = get_field("program_partners", "options");

					if($partners){
						shuffle($partners);
						?>
                        <div class="dark-wrap">
                            <div class="container pr-0 pr-sm-3 pt-5 pb-5">
                                <div id="program-partner" class="owl-carousel owl-theme pb-5 pt-5">
									<?php

									foreach ($partners as $partner){
										get_template_part("template-parts/card/partner");
									}
									?>
                                </div>
                            </div>
                        </div>
						<?php
					}
					?>
                </section>


                <section class="light-wrap pt-large" id="sezione-tech-partner">
                    <div class="container">
                        <div class="txtimg row">
                            <div class="col-12 col-lg-6 order-lg-1 mb-4 mb-5 mb-lg-0">
                                <div class="img-wrap v-offset">
                                    <img class="img-fluid" src="<?php echo get_field("foto_tech_partner"); ?>" alt="<?php echo esc_attr(get_field("titolo_partner")); ?>" />
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 order-lg-0 mb-3">
                                <div class="hgroup">
                                    <h2 class="title"><?php the_field("titolo_tech_partner"); ?></h2>
                                </div>
                                <div class="text">
                                    <p><?php the_field("testo_tech_partner"); ?></p>
                                </div>
                                <div class="undertext mb-2">
									<?php the_field("avvertenze_tech_partner"); ?>
                                </div>
                                <div class="reference mb-5">
									<?php
									if(get_field("link_avviso_tech_partner")){
										?>

                                        <a target="_blank" class="button-link mr-5 mt-4" href="<?php echo get_field("link_avviso_tech_partner"); ?>"><span><?php _e("Visualizza avviso pubblico","madeit"); ?></span></a>
										<?php
									}

									if(get_field("link_allegato_tech_partner")){
										?>
                                        <a class="button-link mt-4" href="<?php echo get_field("link_allegato_tech_partner"); ?>"><span><?php _e("Invia la tua proposta","madeit"); ?></span></a>
										<?php
									}
									?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="dark-wrap pb-5">
						<?php
						unset($partners);
						$partners = get_field("tech_partners", "options");
						if($partners){
							shuffle($partners);

							?>

                            <div class="container pr-0 pr-sm-3 pt-5">
                                <div id="tech-partner" class="owl-carousel owl-theme pb-5 pt-5">
									<?php

									foreach ($partners as $partner){
										get_template_part("template-parts/card/partner");
									}
									?>
                                </div>
                            </div>

							<?php
						}
						?>

                        <div class="container">
                            <div class="row align-items-center pt-5 pb-5">

                                <div class="col-12 col-lg-5 offset-lg-1 mb-3 mb-md-5 mb-lg-0">
                                    <div class="square-list-compact single-counter">
                                        <ol class="main-list text-white">
											<?php
											$elenco = get_field("elenco_contest");
											foreach ($elenco as $item){
												?>
                                                <li>
                                                    <div class="item fs-big">
														<?php
														echo $item["testo"];
														?>
                                                    </div>
                                                </li>
												<?php
											}
											?>
                                        </ol>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4 offset-lg-1">
                                    <div class="row counter-wrap">
                                        <div class="col-6 col-lg-9">
                                            <div class="partner-counter">
                                                <div class="number"><?php  echo wp_count_posts("contest")->publish; ?></div>
                                                <div class="dida">
													<?php _e("Tech contest pubblicati", "madeit"); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 col-lg-9 ml-lg-auto mr-lg-0">
                                            <div class="partner-counter">
                                                <div class="number"><?php
                                                    if($partners)
    													echo count( $partners ) ;
                                                    else
                                                        echo "0";
													?></div>
                                                <div class="dida">
													<?php _e("Partner", "madeit"); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>


                <section class="light-wrap pt-large" id="sezione-supporter-partner">
                    <div class="container">

                        <div class="row txtimg">
                            <div class="col-12 col-lg-6 offset-lg-0 mb-4 mb-5 mb-lg-0">
                                <div class="image-wrap  v-offset">
                                    <img src="<?php echo get_field("foto_supporter_partner"); ?>" class="img-fluid" alt="MadeIT" />
                                </div>
                            </div>
                            <div class="col-12 col-lg-5">
                                <div class="hgroup  mb-4 mb-md-0">
                                    <h2 class="title"><?php the_field("titolo_supporter_partner"); ?></h2>
                                </div>
                                <div class="text">
                                    <p><?php the_field("testo_supporter_partner"); ?></p>
                                </div>
                                <div class="undertext mb-2">
									<?php the_field("avvertenze_supporter_partner"); ?>
                                </div>
                                <div class="reference mb-5">
									<?php
									if(get_field("link_avviso_supporter_partner")){
										?>
                                        <a target="_blank" class="button-link mr-5 mt-4" href="<?php echo get_field("link_avviso_supporter_partner"); ?>"><span><?php _e("Visualizza avviso pubblico","madeit"); ?></span></a>
										<?php
									}
									if(get_field("link_allegato_supporter_partner")){
										?>
                                        <a class="button-link mt-4" href="<?php echo get_field("link_allegato_supporter_partner"); ?>"><span><?php _e("Invia la tua proposta","madeit"); ?></span></a>
										<?php
									}
									?>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
					unset($partners);
					$partners = get_field("supporter_partners", "options");

					if($partners){
						shuffle($partners);
						?>
                        <div class="dark-wrap">
                            <div class="container pr-0 pr-sm-3 pt-5 pb-5">
                                <div id="supporter-partner" class="owl-carousel owl-theme pb-5 pt-5">
									<?php

									foreach ($partners as $partner){
										get_template_part("template-parts/card/partner");
									}
									?>
                                </div>
                            </div>
                        </div>
						<?php
					}
					?>
                </section>

                <section>
                    <div class=" pt-xxlarge">
                        <div class="container pb-xxlarge">
                            <div class="row txtimg">
                                <div class="col-12 col-lg-6 mb-4 mb-5 mb-lg-0">
                                    <div class="img-wrap">
                                        <img class="img-fluid" src="<?php the_field("immagine_cta_partner"); ?>" alt="" />
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="hgroup">
                                        <strong class="supertitle"><?php the_field("occhiello_cta_partner"); ?></strong>
                                        <h2 class="title"><?php the_field("titolo_cta_partner"); ?></h2>
                                    </div>
                                    <div class="reference">
                                        <a target="_blank" class="button large  mb-3 mb-md-0 mr-2 mr-lg-5" href="<?php the_field("link_bottone_cta_partner"); ?>"><?php the_field("label_bottone_cta_partner"); ?></a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>

			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div>


<?php get_footer();


