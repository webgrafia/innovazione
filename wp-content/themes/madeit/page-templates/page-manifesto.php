<?php
/* Template Name: Manifesto */

get_header();
$sezioni = get_field("sezioni");
?>
	<div id="page-content" class="main" role="main">

<?php
global $post;
if ( have_posts() ) :
	/* Start the Loop */
	while ( have_posts() ) : the_post();
		setup_postdata($post);
		?>

        <section class="page-hero" style="background-image:url('<?php echo get_the_post_thumbnail_url($post, "full"); ?>');">
            <div class="container hero-caption">
                <div class="row">
                    <div class="col-12 col-lg-10 col-xl-8">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page"><?php the_field("occhiello"); ?></li>
                            </ol>
                        </nav>
                        <h2 class="title">
	                        <?php the_title(); ?>
                        </h2>
                        <div class="text">
                            <p><?php the_field("sommario"); ?></p>
                        </div>

                        <div class="reference reference-mb-fix  d-md-none">
                            <a target="_blank" class="button" href="<?php echo get_field("link_manifesto", "options"); ?>"><?php echo get_field("testo_link_sezione_manifesto", "options"); ?></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container hero-footer d-none d-md-block">
                <div class="row">
                    <div class="col-12 text-center">
                        <a class="button-down" href="#manifesto">
                            <span class="sr-only"><?php _e("Scopri cosa può fare MadeIT per la tua startup.", "madeit"); ?></span>
                        </a>
                    </div>
                </div>
            </div>

        </section>

        <section class="light-switch">
            <div class="sidebar-wrap sidebar-fix">
                <div class="container-fluid container-sidebar d-none d-md-block">
                    <div class="row justify-content-end">
                        <div class="col col-sidebar">
                            <div class="pin-content pt-large pb-small">
                                <div class="sidebar-index-wrap single-counter">
                                    <strong class="title"><?php the_field("titolo_generale"); ?></strong>
                                    <ol class="index">
                                        <?php
                                        $c=0;
                                        foreach ( $sezioni as $item ) {
                                            $c++;
                                            ?>
                                            <li><a href="#item-<?php echo $c; ?>"><?php echo $item["titolo_menu"]; ?></a></li>
                                        <?php } ?>
                                    </ol>
                                    <br>
                                    <a target="_blank" class="button w-75 text-white" href="<?php echo get_field("link_manifesto", "options"); ?>"><?php echo get_field("testo_link_sezione_manifesto", "options"); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar-main pt-xxlarge pb-xxlarge">
                    <a class="bm" name="manifesto" id="manifesto"></a>
                    <div class="square-list single-counter">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">
                                    <h2 class="title"><?php the_field("titolo_generale"); ?></h2>
                                    <ol class="main-list scroll-list">
	                                    <?php
	                                    $c=0;
	                                    foreach ( $sezioni as $item ) {
		                                    $c++;
		                                    ?>

                                            <li>
                                                <a class="bm-nospace" id="item-<?php echo $c; ?>" name="item-<?php echo $c; ?>"></a>
                                                <div class="item">
                                                    <h3 class="item-title">
	                                                    <?php echo $item["titolo_sezione"]; ?>
                                                    </h3>
                                                    <div class="text fs-big">
	                                                    <?php echo $item["testo"]; ?>
                                                    </div>
                                                </div>
                                            </li>
	                                    <?php } ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


		<?php // get_template_part("template-parts/common/manifesto");  ?>

	<?php
	endwhile;
else :

	get_template_part( 'template-parts/content', '404' );

endif; ?>
    </div>
<?php get_footer();


