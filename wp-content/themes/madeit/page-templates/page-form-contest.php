<?php
/* Template Name: Form Contest */

get_header(); ?>

<style>
    input[type=text],textarea,input[type=date],input[type=url],input[type=email],input[type=tel]{
        font-weight:normal;
    }

    .bootstrap-select-wrapper button .filter-option::after {
        height:0px;
    }

    .bootstrap-select-wrapper   .dropdown.bootstrap-select .btn {
        box-shadow: 0px 4px 3px #ccc;

    }

    .bootstrap-select-wrapper  button.dropdown-toggle::after {
        border:0;
        translate:0;
        position: absolute;
        top: -18px;
        right: 0;
        left: auto;
        height: 39px;
        width: 39px;
        background-color:#0d00ff !important;
        background: url('<?php echo get_template_directory_uri(); ?>/assets/img/arrowdown.png') no-repeat 50% 50%;
        background-size: none;
    }

    ::-webkit-calendar-picker-indicator {
        filter: invert(13%) sepia(70%) saturate(6518%) hue-rotate(249deg) brightness(87%) contrast(155%);

    }

</style>

    <div id="page-content" class="main no-hero" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);
				?>
                <div class="light-wrap pb-xxlarge">
                    <div class="container pt-large pb-5">
                        <div class="row">

                            <div class="col-12 col-xl-10 offset-xl-1">
                                <div class="section-heading top-page">
                                    <h2 class="title pr-0 pr-lg-5" >
										<?php the_title(); ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container">
                        <div class="row cards-v-wrap">

                            <div class="col-12 col-xl-10 offset-xl-1">
                                <div class="card-content-item px-0 pt-4">
									<?php
                                    the_content();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.light-wrap -->
			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div> <!-- page-content //-->

    <script>
        jQuery( document ).ready(function() {
            disable_blocks();
                checkRadio($('.unlock-next input[type="radio"]'));

        });

        function checkRadio(elem){

            elem.change(function(){
                var value = $(this).filter(":checked").val();

                if(value == "SI"){
                    enable_blocks();
                    $( this ).siblings().parent().parent().parent().parent().parent().parent().next().removeClass("disabled");
                    disable_blocks();
                }else{
                    enable_blocks();
                    $( this ).siblings().parent().parent().parent().parent().parent().parent().next().addClass("disabled");
                    disable_blocks();
                }

            });
        }

        function disable_blocks(){
            jQuery("form .disabled input").attr('readonly',true);
            jQuery("form .disabled input").attr('disabled',true);
            jQuery("form .disabled textarea").attr('disabled',true);

        }

        function enable_blocks(){
            jQuery("form .disabled input").attr('readonly',false);
            jQuery("form .disabled input").attr('disabled',false);
            jQuery("form .disabled textarea").attr('disabled',false);
        }

    </script>


<?php get_footer();