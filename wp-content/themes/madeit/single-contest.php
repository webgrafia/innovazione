<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header(); ?>

    <div id="page-content" class="main" role="main">

		<?php
		global $post;
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				setup_postdata($post);


				$inizio_iscrizioni = get_field("inizio_iscrizioni");
				$termine_iscrizioni = get_field("termine_iscrizioni");
				$data_inizio_contest = get_field("data_inizio_contest");
				$data_fine_contest = get_field("data_fine_contest");

				// partner
				$partners = wp_get_object_terms($post->ID, "partner");
				$logo = false;
				if(is_array($partners) && count($partners) > 0){
					$partner = $partners[0];
					$image_p = get_field("logo", $partner);
					if($image_p)
						$logo = $image_p["url"];
				}

				?>
                <section class="page-hero">
                    <div class="container hero-caption">
                        <div class="row">
                            <div class="col-lg-12">
								<?php
								if($logo){
									?>
                                    <div class="logo-wrap">
                                        <img src="<?php echo $logo; ?>" alt="<?php echo esc_attr($partner->name); ?>" class="img-fluid" style="max-width: 270px" />
                                    </div>
									<?php
								}
								?>
                            </div>
                            <div class="col-12 col-lg-8 col-xl-6 pr-xl-5">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item" aria-current="page"><?php _e("MadeIT per le startup", "madeit"); ?></li>
                                        <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo get_post_type_archive_link("contest"); ?>"><?php _e("Tech contest", "madeit"); ?></a></li>
                                    </ol>
                                </nav>
                                <h2 class="title">
									<?php the_title(); ?>
                                </h2>
                                <div class="text">
                                    <p><?php echo get_the_excerpt(); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section>

                    <div class="sidebar-wrap">
                        <div class="container-fluid container-sidebar">
                            <div class="row justify-content-end">
                                <div class="col col-sidebar sidebar-m">
                                    <div class="pin-content pt-xxsmall pb-xxsmall">
                                        <p><strong><?php _e("Contest organizzato da:", "madeit"); ?></strong><br />
											<?php echo $partner->name; ?>
                                        </p>
                                        <p class="mb-1"><strong><?php _e("Principali argomenti:", "madeit"); ?></strong>
                                        </p>
                                        <div class="tag-wrap mb-3">
											<?php
											// argomenti
											$argomenti = wp_get_object_terms($post->ID, "argomento");
											if($argomenti){
												foreach ($argomenti as $argomento){
													echo '<span class="tag">'.$argomento->name.'</span>';
												}
											}
											?>
                                        </div>

                                        <p><strong><?php _e("Data iscrizione:", "madeit"); ?></strong><br />
		                                    dal <?php // echo  date_i18n('D', strtotime($inizio_iscrizioni)); ?>    <?php echo ($inizio_iscrizioni); ?>
                                            al  <?php // echo  date_i18n('D', strtotime($termine_iscrizioni)); ?>    <?php echo ($termine_iscrizioni); ?>
                                        </p>

                                        <p><strong><?php _e("Data contest:", "madeit"); ?></strong><br />
                                        dal <?php // echo  date_i18n('D', strtotime($data_inizio_contest)); ?>    <?php echo ($data_inizio_contest); ?>
                                            al   <?php // echo  date_i18n('D', strtotime($data_fine_contest)); ?>    <?php echo ($data_fine_contest); ?>
                                        </p>

                                        <a class="button mt-3" href="<?php the_field("link_contest"); ?>" target="_blank"><?php _e("Scopri di più", "madeit"); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar-main pt-large pb-xxlarge">
                            <a class="bm" name="manifesto" id="manifesto"></a>
                            <div class="square-list-titled single-counter">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-9 col-lg-8 offset-xl-1 pr-xl-5">
                                            <ol class="main-list">
                                                <li>
                                                    <div class="item">
                                                        <h3 class="item-title">
															<?php _e("Qual è l'obiettivo?", "madeit"); ?>
                                                        </h3>
                                                        <div class="text">
															<?php
															the_content();
															?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="item">
                                                        <h3 class="item-title">
															<?php _e("Come funziona?", "madeit"); ?>
                                                        </h3>
                                                        <div class="text">
															<?php
															the_field("come_funziona");
															?>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="item">
                                                        <h3 class="item-title">
															<?php _e("A chi ci rivolgiamo?", "madeit"); ?>
                                                        </h3>
                                                        <div class="text">
															<?php
															the_field("a_chi_ci_rivolgiamo");
															?>
                                                        </div>
                                                    </div>
                                                </li>

                                            </ol>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

				<?php
				foreach( $argomenti as $argomento ) {
					$argonames[] = $argomento->name;
				}
				$related = get_posts(
					array(
						'post_type' => "contest",
						'numberposts'  => 2,
						'relation' => 'OR',
						'tax_query' => array(
							array(
								'taxonomy' => 'argomento',
								'field'    => 'slug',
								'terms'    => $argonames
							),
						),
						'post__not_in' => array( $post->ID )
					)
				);

				if( $related ) {
					?>
                <section class="pt-large pb-xxlarge">
                    <div class="container">
                        <div class="row">

                            <div class="col-12">
                                <div class="section-heading">
                                    <div class="hgroup">
                                        <h2 class="title">
											<?php _e("Guarda altri tech contest che potrebbero interessarti", "madeit"); ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row card-wrap wrap-xxsmall">
							<?php

								foreach( $related as $post ) {
									setup_postdata($post);
									?>
                                    <div class="col-12 col-md-6 col-lg-4 order-lg-1">
										<?php get_template_part( 'template-parts/card/contest'); ?>
                                    </div>
									<?php
								}
								wp_reset_postdata();
							?>

                            <div class="col-12 col-lg-4 d-flex align-items-center order-lg-0">
                                <a class="button-big-link column" href="<?php echo get_post_type_archive_link("contest"); ?>">
                                    <span class="text"><?php _e("Torna alla sezione dei <br class=\"d-none d-sm-block\" /> tech contest", "madeit"); ?></span>
                                    <br />
                                    <span class="arrow back"></span>
                                </a>
                            </div>

                        </div>

                    </div>
                </section>
	                        <?php
                        }
				?>
			<?php
			endwhile;
		else :

			get_template_part( 'template-parts/content', '404' );

		endif; ?>

    </div><!-- /.page-content -->



<?php get_footer();
