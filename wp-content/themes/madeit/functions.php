<?php
/**
 * madeit functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package madeit
 */


/**
 * Custom Post Type
 */
require get_template_directory() . '/inc/cpt.php';

/**
 * acf
 */
require get_template_directory() . '/inc/acf.php';

/**
 * utils
 */
require get_template_directory() . '/inc/utils.php';


/**
 * Enqueue scripts and styles.
 */
function madeit_assets() {
	$version = 0.1;
	wp_enqueue_style( 'bootstrap-italia', get_template_directory_uri() . "/vendors/bootstrap-italia/css/bootstrap-italia.min.css");
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . "/vendors/fontawesome-free-5.13.0-web/css/all.css");
	wp_enqueue_style( 'template-style', get_template_directory_uri() . "/assets/css/style.css");
	wp_enqueue_style( 'theme-style', get_template_directory_uri() . "/style.css");
	wp_enqueue_style( 'print-style', get_template_directory_uri() . "/print.css", array(), "", "print");

	wp_enqueue_script( 'boostrap-italia', get_template_directory_uri() . '/vendors/bootstrap-italia/js/bootstrap-italia.bundle.min.js', array(), $version, true );
	wp_enqueue_script( 'scrollmagic', get_template_directory_uri() . '/vendors/scrollmagic/minified/ScrollMagic.min.js', array(), $version, true );
	wp_enqueue_script( 'general-javascript', get_template_directory_uri() . '/assets/js/main.js', array(), $version, true );

	if(is_post_type_archive(array("incentivo", "contest"))){
		wp_enqueue_script( 'shufflejs', get_template_directory_uri() . '/vendors/shufflejs/shuffle.js', array(), $version, true );
		wp_enqueue_script( 'filter-shufflejs', get_template_directory_uri() . '/assets/js/filters.js', array(), $version, true );
	}
}
add_action( 'wp_enqueue_scripts', 'madeit_assets' );


/**
 * Registro menu
 */
function madeit_menus() {
	register_nav_menus(
		array(
			'primary-menu' => __( 'Primary Menu' ),
			'footer-1-menu' => __( 'Footer 1' ),
			'footer-2-menu' => __( 'Footer 2' ),
			'subfooter-menu' => __( 'Sub-Footer Menu' ),
		)
	);
}
add_theme_support( 'menus' );
add_action( 'init', 'madeit_menus' );

add_filter('nav_menu_css_class' , 'madeit_nav_special_class' , 10 , 2);
function madeit_nav_special_class($classes, $item){
		$classes[] = 'nav-item';
    return $classes;
}


/**
 * size tagli immagine
 */
add_theme_support( 'post-thumbnails' );

function madeit_image_sizes() {

	add_image_size( 'square', 800, 800, true );
	add_image_size( 'card', 720, 360, true );
	add_image_size( 'content', 1362, 768, true );

}
add_action( 'init', 'madeit_image_sizes' );



/**
 *  Sidebars management
 */
if ( function_exists('register_sidebar') ) {

    register_sidebar(array(
        'name' => 'Post Sidebar',
        'id' => 'post-sidebar',
        'description' => 'Sidebar di dettaglio dei post',
        'before_widget' => '<div  id="%1$s"  class="card   rounded shadow mt-4 mb-3 pt-3 widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4  class="w-100 text-center">',
        'after_title' => '</h4>',
    ));


}



add_action('vsz_cf7_admin_after_heading_field','add_cf7db_col', 11, 2);
function add_cf7db_col(){
	?><th style="width: 32px;" class="manage-column"><?php _e(''); ?></th><?php
}

add_action('vsz_cf7_admin_after_body_field', 'add_cf7db_actions', 21, 2);

function add_cf7db_actions($form_id, $row_id){
	$row_id = (int)$row_id;
	?><td>
	<a  href="?page=<?php echo $_GET["page"]; ?>&cf7_id=<?php echo $_GET["cf7_id"]; ?>&action=creapost&row=<?php echo $row_id; ?>"  class="" name="Crea post">
		<i class="fa fa-plus" title="Crea Contest" aria-hidden="true" style="font-size:20px;"></i>
	</a>
	</td><?php
}