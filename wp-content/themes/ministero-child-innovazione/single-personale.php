<?php
get_header(); ?>

<?php
if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		get_template_part("template-parts/single/breadcrumb");
		?>
		<div class="container my-4">
			<div class="row">
				<div class="col col-8">
					<div class="mx-md-3">
						<h1><?php the_title(); ?></h1>
						<?php
						$posizione_aperta = get_field("posizione_aperta");
						if(!$posizione_aperta){
							?>
							<span class="badge badge-secondary d-inline align-text-top">Posizione chiusa</span>
							<?php
						}
						?>
						<?php the_content(); ?>
					</div>
				</div>
				<div class="c   ol col-4">
				</div>
			</div>
		</div>
	<?php
	endwhile;
endif; ?>

<?php
get_footer();
