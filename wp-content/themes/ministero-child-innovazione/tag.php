<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header();

	global $wp_query;
	$tax = $wp_query->get_queried_object();
	// print_r($tax);
	$immagine_sfondo = get_field("immagine_sfondo", $tax);
	$contenuto = get_field("contenuto", $tax);

	?>


	<section class="innovation-section-head" style="<?php if($immagine_sfondo) echo "background-attachment:fixed;background-position-y: 0px;background-image: url(".$immagine_sfondo["url"].")"  ?>" >
		<div class="container">
			<div class="row">
				<div class="col-lg-10">
					<h1><?php echo single_term_title(); ?></h1>
					<?php the_archive_description( '<p class="lead">', '</p>' ); ?>
				</div>
			</div>
		</div>
	</section>

	<section id="content" role="main" class="container">

		<div class="container">
			<div class="row">

				<?php get_template_part("template-parts/single/breadcrumb"); ?>

					<?php

					echo  "<div class='col-12'><div class='max-w-40'> ".$contenuto."</div></div>";
				if ( have_posts() ) {

					?>
					<div class='col-12 mt-3'>
						<div class='max-w-40'>
							<h2>Le Novità</h2>
							<p>Qui trovi una serie di risorse per conoscere e usare i servizi digitali</p>
						</div>
					</div>
					<?php

					// Load posts loop.
					while ( have_posts() ) {
						the_post();
						echo '<div class="col-12 col-md-4 d-flex mb-3 mt-1">';
						get_template_part( 'template-parts/card/post', get_post_type() );
						echo "</div>";
					}
				} ?>


			</div>

		</div>

	</section>


<?php
$risorse = get_posts(
	array(
		"post_type" => "risorsa",
		"posts_per_page" => "-1",
		'tax_query'      => array(
			array(
				'taxonomy'  => 'post_tag',
				'field'     => 'slug',
				'terms'     => sanitize_title( $tax->slug )
			)
		)
	)
);
if($risorse){

	?>

	<section class="section section-muted">
		<div class="section-content">
			<div class="container">
	<div class='col-12 mt-3'>
		<div class='max-w-40'>
			<h2>Le Risorse</h2>
			<p>Qui trovi una serie di risorse per conoscere e usare i servizi digitali</p>
		</div>
	</div>
	<div class="link-list-wrapper max-w-40 mb-5">
		<ul class="link-list">
			<?php
			foreach ($risorse as $risorsa){

				$link_risorsa = get_field("link_risorsa", $risorsa);
				$classe_icona = get_field("classe_icona", $risorsa);
				?>
				<li>
					<a class="list-item left-icon" target="_blank" rel="noopener noreferrer" href="<?php echo $link_risorsa; ?>">
						<svg class="icon icon-primary icon-sm"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#<?php echo $classe_icona; ?>"></use></svg>
						<span class="pl-2 d-inline"><?php echo $risorsa->post_title; ?></span>
					</a>
				</li>

				<?php

			}
			?>
		</ul>
	</div>
	</div>
		</div>
	</section>
	<div class="innovation-divider--mask"></div>

	<?php
}


$progetti = get_posts(
	array(
		"post_type" => "progetto",
		"posts_per_page" => "-1",
		'tax_query'      => array(
			array(
				'taxonomy'  => 'post_tag',
				'field'     => 'slug',
				'terms'     => sanitize_title( $tax->slug )
			)
		)
	)
);
if($progetti) {
	?>


	<section class="section">
		<div class="section-content">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h2>Le tecnologie abilitanti</h2>
						<p>Qui trovi tutti i progetti e le piattaforme abilitanti su cui stiamo lavorando, per rendere i
							servizi pubblici sempre più efficienti, semplici, progettati intorno ai bisogni dei
							cittadini.</p>
					</div>
					<?php
					foreach ( $progetti as $progetto ) {
						$sigla_progetto = get_field("sigla_progetto", $progetto);


						?>


						<div class="col-sm-6 col-md-4 col-lg-3 text-center">
							<div class="it-grid-item-wrapper it-grid-item-overlay">
								<a href="<?php echo get_permalink($progetto); ?>">
									<div class="img-responsive-wrapper">
										<div class="img-responsive">
											<div class="img-wrapper">
												<picture>
													<?php echo get_the_post_thumbnail($progetto->ID, "square" ); ?>
												</picture>
											</div>
										</div>
									</div>
									<div class="it-griditem-text-wrapper">
										<span class="it-griditem-text"><?php echo $sigla_progetto; ?></span>
									</div>
								</a>
							</div>
							<div class="my-3 mx-5">
								<p class="x-small"><?php echo $progetto->post_title; ?></p>
							</div>
						</div>
						<?php
					}
					?>

				</div>
			</div>
		</div>
	</section>

	<?php

}
	?>


<?php get_footer();
