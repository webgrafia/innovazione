<?php
/* Template Name: Italia 2025 */

get_header();

if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		if ( has_post_thumbnail()) {
			$immagine_sfondo = get_the_post_thumbnail_url( $post, "full" );
		}
		?>


        <section class="innovation-section-head"  style="<?php if($immagine_sfondo) echo "background-attachment:fixed;background-position-y: 0px;background-image: url(".$immagine_sfondo.")"  ?>" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 text-shadow-green">
                        <h1><?php the_title(); ?></h1>
                        <p class="lead"><?php echo strip_tags(get_the_content()); ?></p>
                    </div>
                </div>
            </div>
        </section>

		<?php
		get_template_part("template-parts/single/breadcrumb");
		?>

        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1">
					<?php
					$immagine_2025 = get_field("immagine_2025", "options");
					?>
                    <img class="img-fluid" alt="Avanzamento obiettivi Italia 2025" src="<?php echo $immagine_2025["url"]; ?>">
                </div>
            </div>
        </div>



        <div class="container my-4">
            <div class="row">
                <div class="col col-lg-8">
                    <div class="mx-md-3">
                        <h2 id="strategia">La Strategia</h2>
						<?php echo get_field("strategia", "options"); ?>
                    </div>
                </div>

                <div class="col-12 col-lg-4 sidebar">
                    <nav class="navbar it-navscroll-wrapper navbar-expand-lg it-bottom-navscroll it-left-side affix-top">
                        <button class="custom-navbar-toggler" type="button" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation" data-target="#navbarNav"><span class="it-list"></span>Italia 2025
                        </button>
                        <div class="navbar-collapsable" id="navbarNav">
                            <div class="overlay"></div>
                            <div class="close-div sr-only">
                                <button class="btn close-menu" type="button"><span class="it-close"></span>Chiudi</button>
                            </div>
                            <a class="it-back-button" href="#">
                                <svg class="icon icon-sm icon-primary align-top">
                                    <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-chevron-left"></use>
                                </svg>
                                <span>Indietro</span></a>
                            <div class="menu-wrapper">

                                <div class="link-list-wrapper">
                                    <ul class="link-list section-nav">
                                        <li class="toc-entry toc-h3"><a href="#strategia">La Strategia</a></li>
                                        <li class="toc-entry toc-h3"><a href="#azioni">Le Azioni</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>







<?php
    $azioni = get_field("azioni","options");
    if($azioni) {

	    ?>


        <div id="punti-accordion">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 id="azioni" class="mt-4">Le azioni</h2>
                    </div>
                </div>
                <div class="row">
                    <?php
                    $i=0;
                    foreach ($azioni as $azione){
                       // print_r($azione);

                        ?>
                    <div class="col-12 col-lg-6">
                        <?php if($azione["attivo"]){ ?>
                            <div class="card no-after shadow my-2 ">
                                <div class="card-toggle" data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>"
                                     aria-expanded="false" aria-controls="collapse-<?php echo $i; ?>">
                                    <figure class="overlay-wrapper my-0">
                                        <img src="<?php echo $azione["immagine"]["url"]; ?>" alt="Immagine" class="img-fluid"/>
                                        <figcaption class="overlay-panel overlay-panel-fullheight overlay-black"><a
                                                    href="<?php echo $azione["url"]; ?>"
                                                    class="text-white" target="_blank" style="z-index: 100"
                                                    onclick="event.stopPropagation();"><?php echo $azione["codice"]; ?></a></figcaption>
                                    </figure>
                                    <div class="card-img-overlay text-white">
                                        <h4 class="card-title"><?php echo $azione["titolo"]; ?></h4>
                                        <p class="card-text small"><?php echo $azione["descrizione"]; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <p class="small justify-content-center align-self-center d-inline-block">
                                            <a class="read-more read-more--relative mx-3 mt-2 collapsed" href="#"
                                               data-toggle="collapse" data-target="#collapse-<?php echo $i; ?>" aria-expanded="false"
                                               aria-controls="collapse-<?php echo $i; ?>">
                                                <span class="read-more__read">
                                                    <span class="text">Leggi di più</span>
                                                    <svg class="icon">
                                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-arrow-right"></use>
                                                    </svg>
                                                </span>
                                                <span class="read-more__close">
                                                    <span class="text">Chiudi</span>
                                                    <svg class="icon">
                                                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-close"></use>
                                                    </svg>
                                                </span>
                                            </a>
                                        </p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div id="collapse-<?php echo $i; ?>" class="collapse" aria-labelledby="headingOne"
                                             data-parent="#punti-accordion">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3 class="p-3"><?php echo $azione["titolo"]; ?></h3>
                                                    <div class="my-2 p-3 x-small">
                                                        <h4>Obiettivo</h4>
	                                                    <?php echo $azione["obiettivo"]; ?>
                                                        <h4>Chi</h4>
	                                                    <?php echo $azione["chi"]; ?>
                                                        <h4>Come</h4>
	                                                    <?php echo $azione["come"]; ?>
                                                        <h4>Quando</h4>
	                                                    <?php echo $azione["quando"]; ?>
                                                        <h4>Target</h4>
	                                                    <?php echo $azione["target"]; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="card no-after shadow my-2 card--disabled">
                                <div class="card-toggle">
                                    <figure class="overlay-wrapper my-0">
                                        <img src="<?php echo $azione["immagine"]["url"]; ?>" alt="Immagine" class="img-fluid"/>
                                        <figcaption class="overlay-panel overlay-panel-fullheight overlay-black"><span
                                                    class="sr-only"><?php echo $azione["codice"]; ?></span></figcaption>
                                    </figure>
                                    <div class="card-img-overlay text-white">
                                        <h4 class="card-title"><?php echo $azione["titolo"]; ?></h4>
                                        <p class="card-text small"><?php echo $azione["descrizione"]; ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <p class="small justify-content-center align-self-center d-inline-block">
                                            <span class="read-more read-more--relative mx-3 mt-2 d-inline-block">Coming soon</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                        <?php
                        $i++;
                    }
                    ?>

                </div>
            </div>
        </div>
	    <?php
    }
        ?>

        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
	                <?php
	                $logo_2025 = get_field("logo_2025", "options");
	                ?>
                    <img class="d-inline-block my-5" src="<?php echo $logo_2025["url"]; ?>" alt="Logo Italia 2025" style="max-width:200px" />
                </div>
            </div>
        </div>



	<?php



	endwhile;
endif;
?>


<?php get_footer();


