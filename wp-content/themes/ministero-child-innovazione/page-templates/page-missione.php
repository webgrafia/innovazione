<?php
/* Template Name: Missione */

get_header();

if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		if ( has_post_thumbnail()) {
			$immagine_sfondo = get_the_post_thumbnail_url( $post, "full" );
		}
		?>


		<section class="innovation-section-head"  style="<?php if($immagine_sfondo) echo "background-attachment:fixed;background-position-y: 0px;background-image: url(".$immagine_sfondo.")"  ?>" >
			<div class="container">
				<div class="row">
					<div class="col-lg-10 text-shadow-green">
						<h1><?php the_title(); ?></h1>
						<p class="lead"><?php echo strip_tags(get_the_content()); ?></p>
					</div>
				</div>
			</div>
		</section>

		<?php
		get_template_part("template-parts/single/breadcrumb");
		?>



		<div class="container my-4">
			<div class="row">
				<div class="col col-lg-8">
					<div class="mx-md-3">
						<h2 id="visione">Visione</h2>
						<?php echo get_field("visione", "options"); ?>
						<h2 id="missione">Missione</h2>
						<?php echo get_field("missione", "options"); ?>



					</div>
				</div>

				<div class="col-12 col-lg-4 sidebar">
					<nav class="navbar it-navscroll-wrapper navbar-expand-lg it-bottom-navscroll it-left-side affix-top">
						<button class="custom-navbar-toggler" type="button" aria-controls="navbarNav" aria-expanded="false"
						        aria-label="Toggle navigation" data-target="#navbarNav"><span class="it-list"></span>Cosa Facciamo
						</button>
						<div class="navbar-collapsable" id="navbarNav">
							<div class="overlay"></div>
							<div class="close-div sr-only">
								<button class="btn close-menu" type="button"><span class="it-close"></span>Chiudi</button>
							</div>
							<a class="it-back-button" href="#">
								<svg class="icon icon-sm icon-primary align-top">
									<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-chevron-left"></use>
								</svg>
								<span>Indietro</span></a>
							<div class="menu-wrapper">

								<div class="link-list-wrapper">
									<ul class="link-list section-nav">
										<li class="toc-entry toc-h3"><a href="#visione">Visione</a></li>
										<li class="toc-entry toc-h3"><a href="#missione">Missione</a></li>
                                        <li class="toc-entry toc-h3"><a href="#progetti">Progetti</a></li>
                                        <li class="toc-entry toc-h3"><a href="#manifesto">Manifesto</a></li>
									</ul>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>


		<?php
		$progetti = get_posts(
			array(
				"post_type" => "progetto",
				"posts_per_page" => "-1",
			)
		);
		if($progetti) {
			?>
			<section class="section section-muted">
				<div class="section-content">
					<div class="container">
						<div class="row">
							<div class="col-12 text-center">
								<h2  id="progetti">I progetti del dipartimento</h2>
								<p>Qui trovi tutti i progetti e le piattaforme abilitanti su cui stiamo lavorando, per rendere i
									servizi pubblici sempre più efficienti, semplici, progettati intorno ai bisogni dei
									cittadini.</p>
							</div>
							<?php
							foreach ( $progetti as $progetto ) {
								$sigla_progetto = get_field("sigla_progetto", $progetto);


								?>


								<div class="col-sm-6 col-md-4 col-lg-3 text-center">
									<div class="it-grid-item-wrapper it-grid-item-overlay">
										<a href="<?php echo get_permalink($progetto); ?>">
											<div class="img-responsive-wrapper">
												<div class="img-responsive">
													<div class="img-wrapper">
														<picture>
															<?php echo get_the_post_thumbnail($progetto->ID, "square" ); ?>
														</picture>
													</div>
												</div>
											</div>
											<div class="it-griditem-text-wrapper">
												<span class="it-griditem-text"><?php echo $sigla_progetto; ?></span>
											</div>
										</a>
									</div>
									<div class="my-3 mx-5">
										<p class="x-small"><?php echo $progetto->post_title; ?></p>
									</div>
								</div>
								<?php
							}
							?>

						</div>
					</div>
				</div>
			</section>




			<div class="container">
			<section class="manifesto" id="manifesto">
				<?php echo get_field("manifesto", "options"); ?>
			</section>
			</div>
			<?php

		}




	endwhile;
endif;
?>


<?php get_footer();


