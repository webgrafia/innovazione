<?php
/* Template Name: Mediaroom  */

get_header(); ?>

<?php
if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		?>

        <div class="container my-4">
            <div class="row">
                <div class="col col-12">
                    <div class="mx-md-3">
                        <h1 class="text-primary mb-4"><?php the_title(); ?></h1>
						<?php the_content(); ?>


                    </div>
                </div>

                <?php
                $media = get_field("media");
                foreach ($media as $item) {
	                ?>
	                <div class="col-12 col-lg-4 mt-2">
		                <!--start card-->
		                <div class="card-wrapper shadow ">
			                <div class="card card-img no-after">
				                <div class="img-responsive-wrapper">
					                <div class="img-responsive">
						                <figure class="img-wrapper">
							                <img src="<?php echo $item["immagine"]["url"]; ?>">
						                </figure>
					                </div>
				                </div>
				                <div class="card-body">
					                <h5 class="card-title"><?php echo $item["titolo"]; ?></h5>
					                <p class="card-text">
						                <a href="<?php echo $item["link"]; ?>" class="btn btn-primary  btn-icon">
                                        <span class="rounded-icon">
                                            <svg class="icon icon-success">
	                                            <?php
	                                            if ( $item["cta"] == "download" ) {
		                                            ?>
		                                            <use
			                                            xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-arrow-down"></use>
		                                            <?php
	                                            } else {
		                                            ?>
		                                            <use
			                                            xlink:href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap-italia/svg/sprite.svg#it-arrow-right"></use>
		                                            <?php
	                                            }
	                                            ?>
                                            </svg>
                                        </span>
							                <span class="text-white"><?php
								                if ( $item["cta"] == "download" ) {
									                _e( "Download" );
								                } else {
									                _e( "Read more" );
								                }
								                ?></span>
						                </a>
					                </p>
				                </div>
			                </div>
		                </div>
		                <!--end card-->
	                </div>
	                <?php
                }
	                ?>

            </div>
        </div>
	<?php
	endwhile;
endif; ?>

<?php
get_footer();
