<?php

/**
 * Custom Post Type
 */
require get_theme_file_path() . '/inc/cpt.php';

/**
 * acf
 */
require get_theme_file_path() . '/inc/acf.php';

/**
 * shortcode
 */
require get_theme_file_path() . '/inc/shortcode.php';

// aggiungo css child dopo quello del parent
add_action( 'wp_enqueue_scripts', 'innovazione_enqueue_styles', 99 );

function innovazione_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_theme_file_uri().'/style.css' );
}


add_action('pre_get_posts', 'mid_posts_per_tag_archive_page');

function mid_posts_per_tag_archive_page( $query ){
	if ( $query->is_tag() ) {
		$query->set( 'posts_per_page', -1 );
	}
}
