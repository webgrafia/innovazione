<div class="section section-pa bg-radial-blue pt-5">

	<div class="container mt-5">
		<div class="row">
			<div class="col-12">
				<h6 class="text-uppercase text-white-80 max-w-40">Innovazione per la Pubblica Amministrazione</h6>
				<h2 class="text-white max-w-40">Strumenti per servizi digitali efficienti che mettono il cittadino al centro</h2>
			</div>

			<div class="col-12 col-md-4 text-center text-white">
				<div class="m-5">
					<div class="my-4 avatar avatar-dark size-xxl" style="background-color: #143C85">
						<img class="avatar-not-cover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/collaboration.svg" alt="Immagine per Collaborazione">
					</div>
					<h3>Collaborazione</h3>
					<p class="m-2 x-small">Strumenti di collaborazione per coinvolgere enti locali, centrali e fornitori</p>
				</div>
			</div>

			<div class="col-12 col-md-4 text-center text-white">
				<div class="m-5">
					<div class="my-4 avatar avatar-dark size-xxl" style="background-color: #143C85">
						<img class="avatar-not-cover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/technology.svg" alt="Immagine per Tecnologie abilitanti">
					</div>
					<h3>Tecnologie abilitanti</h3>
					<p class="m-2 x-small">Le infrastrutture digitali della Pubblica Amministrazione</p>
				</div>
			</div>

			<div class="col-12 col-md-4 text-center text-white">
				<div class="m-5">
					<div class="my-4 avatar avatar-dark size-xxl" style="background-color: #143C85">
						<img class="avatar-not-cover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/quality.svg" alt="Immagine per Software e standard">
					</div>
					<h3>Software e standard</h3>
					<p class="m-2 x-small">Software open source, design system e standard di interoperabilità</p>
				</div>
			</div>

			<div class="col-12 mt-3 text-center">
				<a class="btn btn-outline-primary bg-white" href="<?php echo get_term_link(get_term_by('slug', "pubblica-amministrazione", 'post_tag')); ?>">Per saperne di più</a>
			</div>
		</div>
	</div>
</div><?php
