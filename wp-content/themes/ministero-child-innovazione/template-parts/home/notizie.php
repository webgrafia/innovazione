<?php
/**
 * ultimo post in apertura
 */
// The Query
global $idapertura;

$args = array(
	'posts_per_page' => 6,
	'post__not_in' => array($idapertura),
	// 'offset' => 1
);
$notizie_loop = new WP_Query($args);
/* Start the Loop */
$c=0;
if ( $notizie_loop->have_posts() ) :


	?>

    <div class="container">
        <div class="row">
            <?php
            /* Start the Loop */
            while ( $notizie_loop->have_posts() ) : $notizie_loop->the_post();
	            if($c == 6) break;
	            $c++;
                echo '<div class="col-12 col-md-4 d-flex">';
                get_template_part( 'template-parts/card/post' );
                echo "</div>";
            endwhile;
            ?>
            <div class="col-12 text-center mt-4 mb-5">
                <a class="btn btn-outline-primary bg-white" href="<?php echo mid_slug_all_posts_link(); ?>">Vedi tutte le Notizie</a>
            </div>
        </div>
    </div><?php

endif;
