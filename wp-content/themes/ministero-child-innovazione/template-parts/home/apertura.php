<?php
/**
 * ultimo post in apertura
 */
// The Query
$args = array(
	'posts_per_page' => 1,
);
//query_posts( $args );
$apertura_loop = new WP_Query($args);


global $idapertura;

if ( $apertura_loop->have_posts() ) :
	/* Start the Loop */
	$c=0;
	while ( $apertura_loop->have_posts() ) : $apertura_loop->the_post();

		if($c) break;
		$c++;
		$idapertura = get_the_ID();
		?>
		<div class="it-hero-wrapper it-dark it-overlay it-bottom-overlapping-content">
		<?php if ( has_post_thumbnail()) {
			$image = get_the_post_thumbnail( $post, "full" );
		}else {
			$image = "<img src='".get_stylesheet_directory_uri()."/img/placeholder.png' />";
		}

		    ?>
			<!-- - img-->
			<div class="img-responsive-wrapper">
				<div class="img-responsive">
					<div class="img-wrapper">
						<?php echo $image; ?>
					</div>
				</div>
			</div>

		<!-- - texts-->
		<div class="container">
			<div class="row">


				<div class="col-12">
<!--
					<div class="row float-none float-lg-right col-12 col-lg-4 mt-2">

						<div class="col-12 col-md-6 card-wrapper card-spacecard-hero" style="height: auto!important;">
							<div class="card card-bg rounded shadow no-after ">
								<div class="card-body text-center">
									<p class="card-text">
										<a href="#" class=""><img class="hero-logo d-inline-block" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-2025.png" alt="Logo Italia 2025"></a>
										<br><span class="d-inline-block mt-2">La strategia per l’innovazione e la trasformazione digitale del Paese</span>
									</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 card-wrapper card-spacecard-hero" style="height: auto!important;">
							<div class="card card-bg rounded shadow no-after ">
								<div class="card-body text-center">
									<p class="card-text">
										<a href="#"><img class="hero-logo d-inline-block" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-repubblica-digitale.svg" alt="Logo Repubblica Digitale"></a>
										<br><span class="d-inline-block mt-2">Un progetto contro il digital divide, per una trasformazione digitale che non lasci indietro nessuno</span>
									</p>
								</div>
							</div>
						</div>
					</div>
//-->
					<div class="it-hero-text-wrapper bg-dark  pt-lg-5 mt-lg-5 mt-0 pt-0">
						<span class="text-white it-category"><?php the_category(","); ?></span>
						<h1 class="text-white"><?php the_title(); ?></h1>
						<p class="text-white d-none d-lg-block">
							<?php echo get_the_excerpt(); ?>
						</p>
						<div class="it-btn-container">
							<a class="btn btn-sm btn-outline-primary" href="<?php the_permalink(); ?>">Leggi di più</a>
						</div>
					</div>

				</div>

			</div>
		</div>
		</div><?php

	endwhile;
endif;
