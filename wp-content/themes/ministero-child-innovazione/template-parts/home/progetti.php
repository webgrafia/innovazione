<?php
$progetti = get_field("progetti_homepage", "options");
if($progetti){
    ?>


    <div class="section px-0 pb-0">
        <div class="container mb-4">
            <div class="row">
                <div class="col-12">
                    <h6 class="text-uppercase text-600 max-w-40">Innovazione per i cittadini e imprese</h6>
                    <h2 class="max-w-40">Informazioni, risorse e punti di accesso per i servizi pubblici digitali</h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <?php
                $c=0;
                foreach ($progetti as $progetto){

	                $sigla_progetto = get_field("sigla_progetto", $progetto);

	                $c++;
                    if($c==1)
                        $class = "col-lg-12";
                    else if(($c==2) || ($c==3) || ($c==4))
	                    $class = "col-lg-4";
                    else
	                    $class = "col-lg-6";

                    $immagine = get_the_post_thumbnail_url($progetto, "full");

                    ?>
                    <div class="<?php echo $class; ?> col-md-6">
                        <a href="<?php echo get_permalink($progetto); ?>" style="background: linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), url('<?php echo $immagine ?>');background-position: center center;background-size: cover;" class="gallery-item mb-3 ">
                            <div class="gallery-item-inner">
                                <div class="gallery-item-inner-text">
                                    <h2><?php echo $sigla_progetto; ?></h2>
                                    <p><?php echo get_the_excerpt($progetto);  ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="container my-5">
            <div class="row">
                <div class="col-12 text-center">
                    <a class="btn btn-primary m-1" href="<?php echo get_term_link(get_term_by('slug', "imprese", 'post_tag')); ?>">Innovazione per le imprese</a>
                    <a class="btn btn-primary m-1" href="<?php echo get_term_link(get_term_by('slug', "cittadini", 'post_tag')); ?>">Innovazione per i cittadini</a>
                </div>
            </div>
        </div>
    </div>


<?php
}