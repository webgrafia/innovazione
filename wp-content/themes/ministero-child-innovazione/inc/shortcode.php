<?php


/**
 * lista posizioni aperte
 * @return string
 */
function innovazione_posizioni_aperte_shortcode() {

	$string = '<h3 class="mt-4" id="posizioni-aperte">Posizioni aperte</h3>';
	$string .= '<div class="jobpositions" id="openjobs">';

	$args = array(
		'post_type' => 'personale',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'posizione_aperta',
				'value' => '1',
				'compare' => '='
			)
		)
	);
	$posts = get_posts($args);
	if($posts) {
		foreach ( $posts as $post ) {
			$string .= '<div>
    <a href="' . get_permalink( $post ) . '" title="' . esc_attr( $post->post_title ) . '"><span class="font-weight-bold">' . $post->post_title . '</span></a>
    <span class="small">&nbsp;Posizione aperta</span>
  </div><hr />';

		}
	}else{
	$string .= '<p>Nessuna posizione aperta</p>';
	}

	$string .= '</div>';

	return $string;

}
add_shortcode('posizioni_aperte', 'innovazione_posizioni_aperte_shortcode');


/**
 * lista posizioni chiuse
 * @return string
 */
function innovazione_posizioni_chiuse_shortcode() {

	$string = '<h3 class="mt-4" id="posizioni-chiuse">Posizioni chiuse</h3>';
	$args = array(
		'post_type' => 'personale',
		'posts_per_page' => -1,
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key'     => 'posizione_aperta',
				'value'   => '0',
				'compare' => '='
			),
			array(
				'key'     => 'posizione_aperta',
				'compare' => 'NOT EXISTS'
			)
		)
	);
	$posts = get_posts($args);
	if($posts) {


		$string .= '<p><a class="btn btn-primary btn-sm" role="button" data-toggle="collapse" href="#jobsarchive" aria-expanded="false" aria-controls="jobsarchive" id="jobsarchive-collapse">
  <span class="seeall">Mostra</span>
  <span class="hideall">Nascondi</span>
</a></p>';
		$string .= '<div class="jobpositions archived collapse" id="jobsarchive">';


		foreach ( $posts as $post ) {
			$string .= '<div>
    <a href="' . get_permalink( $post ) . '" title="' . esc_attr( $post->post_title ) . '"><span class="font-weight-bold">' . $post->post_title . '</span></a>
    <span class="small">&nbsp;Posizione chiusa</span>
  </div><hr />';

		}
	}else{
		$string .= '<p>Nessuna posizione chiusa</p>';
	}
	$string .= '</div>';

	return $string;

}
add_shortcode('posizioni_chiuse', 'innovazione_posizioni_chiuse_shortcode');