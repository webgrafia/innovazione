<?php
if ( ! function_exists('mid_cpt') ) {

// Register Custom Post Type
    function mid_cpt() {

        $labels = array(
            'name'                  => _x( 'Progetti', 'Post Type General Name', 'min_innovazione' ),
            'singular_name'         => _x( 'Progetto', 'Post Type Singular Name', 'min_innovazione' ),
            'menu_name'             => __( 'Progetti', 'min_innovazione' ),
            'name_admin_bar'        => __( 'Progetto', 'min_innovazione' ),
            'archives'              => __( 'Archivio', 'min_innovazione' ),
            'attributes'            => __( 'Attributi', 'min_innovazione' ),
            'parent_item_colon'     => __( 'Parent Item:', 'min_innovazione' ),
            'all_items'             => __( 'Tutti i progetti', 'min_innovazione' ),
            'add_new_item'          => __( 'Aggiungi nuovo progetto', 'min_innovazione' ),
            'add_new'               => __( 'Aggiungi nuovo', 'min_innovazione' ),
            'new_item'              => __( 'Nuovo progetto', 'min_innovazione' ),
            'edit_item'             => __( 'Modifica Progetto', 'min_innovazione' ),
            'update_item'           => __( 'Aggiorna Progetto', 'min_innovazione' ),
            'view_item'             => __( 'Visualizza progetto', 'min_innovazione' ),
            'view_items'            => __( 'Visualizza progetti', 'min_innovazione' ),
            'search_items'          => __( 'Cerca progetti', 'min_innovazione' ),
            'not_found'             => __( 'Non trovato', 'min_innovazione' ),
            'not_found_in_trash'    => __( 'Non trovato nel cestino', 'min_innovazione' ),
            'featured_image'        => __( 'Immagine in evidenza', 'min_innovazione' ),
            'set_featured_image'    => __( 'Definisci immagine in evidenza', 'min_innovazione' ),
            'remove_featured_image' => __( 'Rimuovi immagine', 'min_innovazione' ),
            'use_featured_image'    => __( 'Usa come immagine in evidenza', 'min_innovazione' ),
            'insert_into_item'      => __( 'Inserisci', 'min_innovazione' ),
            'uploaded_to_this_item' => __( 'Caricato sul progetto', 'min_innovazione' ),
            'items_list'            => __( 'Lista progetti', 'min_innovazione' ),
            'items_list_navigation' => __( 'Navigazione progetti', 'min_innovazione' ),
            'filter_items_list'     => __( 'Primo progetto', 'min_innovazione' ),
        );
        $args = array(
            'label'                 => __( 'Progetto', 'min_innovazione' ),
            'description'           => __( 'Progetti', 'min_innovazione' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'thumbnail',  'excerpt'),
            'taxonomies'            => array( 'category', 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_rest'          => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-index-card',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'progetto', $args );


	    $labels = array(
		    'name'                  => _x( 'Risorse', 'Post Type General Name', 'min_innovazione' ),
		    'singular_name'         => _x( 'Risorsa', 'Post Type Singular Name', 'min_innovazione' ),
		    'menu_name'             => __( 'Risorse', 'min_innovazione' ),
		    'name_admin_bar'        => __( 'Risorsa', 'min_innovazione' ),
		    'archives'              => __( 'Archivio', 'min_innovazione' ),
		    'attributes'            => __( 'Attributi', 'min_innovazione' ),
		    'parent_item_colon'     => __( 'Parent Item:', 'min_innovazione' ),
		    'all_items'             => __( 'Tutti ', 'min_innovazione' ),
		    'add_new_item'          => __( 'Aggiungi nuovo', 'min_innovazione' ),
		    'add_new'               => __( 'Aggiungi nuovo', 'min_innovazione' ),
		    'new_item'              => __( 'Nuova Risorsa', 'min_innovazione' ),
		    'edit_item'             => __( 'Modifica Risorsa', 'min_innovazione' ),
		    'update_item'           => __( 'Aggiorna Risorsa', 'min_innovazione' ),
		    'view_item'             => __( 'Visualizza Risorsa', 'min_innovazione' ),
		    'view_items'            => __( 'Visualizza Risorse', 'min_innovazione' ),
		    'search_items'          => __( 'Cerca Risorsa', 'min_innovazione' ),
		    'not_found'             => __( 'Non trovato', 'min_innovazione' ),
		    'not_found_in_trash'    => __( 'Non trovato nel cestino', 'min_innovazione' ),
		    'featured_image'        => __( 'Immagine in evidenza', 'min_innovazione' ),
		    'set_featured_image'    => __( 'Definisci immagine in evidenza', 'min_innovazione' ),
		    'remove_featured_image' => __( 'Rimuovi immagine', 'min_innovazione' ),
		    'use_featured_image'    => __( 'Usa come immagine in evidenza', 'min_innovazione' ),
		    'insert_into_item'      => __( 'Inserisci', 'min_innovazione' ),
		    'uploaded_to_this_item' => __( 'Caricato sul progetto', 'min_innovazione' ),
		    'items_list'            => __( 'Lista progetti', 'min_innovazione' ),
		    'items_list_navigation' => __( 'Navigazione  Risorsa', 'min_innovazione' ),
		    'filter_items_list'     => __( 'Primo Risorsa', 'min_innovazione' ),
	    );
	    $args = array(
		    'label'                 => __( 'Risorsa', 'min_innovazione' ),
		    'description'           => __( 'Risorse', 'min_innovazione' ),
		    'labels'                => $labels,
		    'supports'              => array( 'title'),
		    'taxonomies'            => array( 'post_tag' ),
		    'hierarchical'          => false,
		    'public'                => true,
		    'show_ui'               => true,
		    'show_in_rest'          => true,
		    'show_in_menu'          => true,
		    'menu_position'         => 5,
		    'menu_icon'             => 'dashicons-admin-links',
		    'show_in_admin_bar'     => true,
		    'show_in_nav_menus'     => true,
		    'can_export'            => true,
		    'has_archive'           => true,
		    'exclude_from_search'   => false,
		    'publicly_queryable'    => true,
		    'capability_type'       => 'page',
	    );
	    register_post_type( 'risorsa', $args );



	    $labels = array(
		    'name'                  => _x( 'Personale', 'Post Type General Name', 'min_innovazione' ),
		    'singular_name'         => _x( 'Personale', 'Post Type Singular Name', 'min_innovazione' ),
		    'menu_name'             => __( 'Personale', 'min_innovazione' ),
		    'name_admin_bar'        => __( 'Personale', 'min_innovazione' ),
		    'archives'              => __( 'Archivio', 'min_innovazione' ),
		    'attributes'            => __( 'Attributi', 'min_innovazione' ),
		    'parent_item_colon'     => __( 'Parent Item:', 'min_innovazione' ),
		    'all_items'             => __( 'Tutte le richieste', 'min_innovazione' ),
		    'add_new_item'          => __( 'Aggiungi nuova richiesta', 'min_innovazione' ),
		    'add_new'               => __( 'Aggiungi nuovo', 'min_innovazione' ),
		    'new_item'              => __( 'Nuovo Personale', 'min_innovazione' ),
		    'edit_item'             => __( 'Modifica Personale', 'min_innovazione' ),
		    'update_item'           => __( 'Aggiorna Personale', 'min_innovazione' ),
		    'view_item'             => __( 'Visualizza Personale', 'min_innovazione' ),
		    'view_items'            => __( 'Visualizza Personale', 'min_innovazione' ),
		    'search_items'          => __( 'Cerca Personale', 'min_innovazione' ),
		    'not_found'             => __( 'Non trovato', 'min_innovazione' ),
		    'not_found_in_trash'    => __( 'Non trovato nel cestino', 'min_innovazione' ),
		    'featured_image'        => __( 'Immagine in evidenza', 'min_innovazione' ),
		    'set_featured_image'    => __( 'Definisci immagine in evidenza', 'min_innovazione' ),
		    'remove_featured_image' => __( 'Rimuovi immagine', 'min_innovazione' ),
		    'use_featured_image'    => __( 'Usa come immagine in evidenza', 'min_innovazione' ),
		    'insert_into_item'      => __( 'Inserisci', 'min_innovazione' ),
		    'uploaded_to_this_item' => __( 'Caricato sul Personale', 'min_innovazione' ),
		    'items_list'            => __( 'Lista Personale', 'min_innovazione' ),
		    'items_list_navigation' => __( 'Navigazione Personale', 'min_innovazione' ),
		    'filter_items_list'     => __( 'Primo Personale', 'min_innovazione' ),
	    );
	    $args = array(
		    'label'                 => __( 'Personale', 'min_innovazione' ),
		    'description'           => __( 'Personale', 'min_innovazione' ),
		    'labels'                => $labels,
		    'supports'              => array( 'title', 'editor', 'thumbnail',  'excerpt'),
		    'taxonomies'            => array( 'category', 'post_tag' ),
		    'hierarchical'          => false,
		    'public'                => true,
		    'show_ui'               => true,
		    'show_in_rest'          => true,
		    'show_in_menu'          => true,
		    'menu_position'         => 5,
		    'menu_icon'             => 'dashicons-businessman',
		    'show_in_admin_bar'     => true,
		    'show_in_nav_menus'     => true,
		    'can_export'            => true,
		    'has_archive'           => true,
		    'exclude_from_search'   => false,
		    'publicly_queryable'    => true,
		    'capability_type'       => 'page',
	    );
	    register_post_type( 'personale', $args );


    }
    add_action( 'init', 'mid_cpt', 0 );

}