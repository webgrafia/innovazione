<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gutenberg-starter-theme
 */

get_header(); ?>

<?php
if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();
	$logo = get_field("logo");
?>

		<div class="section section-muted project-intro">
			<div class="section-content">
				<div class="container my-4">
					<div class="row">
						<div class="col-12">
							<div class="d-flex justify-content-between">
								<div class="project-intro--logo mw-100">

									<img src="<?php echo $logo["url"]; ?>" class="rounded img-fluid max-h-80 mb-4">

									<h1 class="max-w-40 text-600"><?php the_title(); ?></h1>
									<p class="lead max-w-40 text-muted"><?php echo get_the_excerpt(); ?></p>
									<?php
									$url_progetto = get_field("url_progetto");
									$sigla_progetto = get_field("sigla_progetto");

									if($url_progetto) {
										?>
										<br><a class="btn btn-primary btn-sm btn-icon"
										       href="<?php echo $url_progetto; ?>" title="<?php echo esc_attr(get_the_title()); ?>" target="_blank"
										       rel="noopener noreferrer">Sito ufficiale</a>
										<?php
									}
										?>
								</div>
							</div>
						</div>
					</div>
					<span class="project-intro--print"><?php echo $sigla_progetto; ?></span>
				</div>
			</div>
		</div>
		<div class="container my-4">

		<?php
		get_template_part("template-parts/single/breadcrumb");
		?>
		<div class="row">
		<div class="col-12 my-4 content">
		<?php the_content(); ?>
		</div>
		</div>
		</div>

	<?php
	endwhile;

else :

	get_template_part( 'template-parts/content', 'none' );

endif; ?>

<?php
get_footer();
